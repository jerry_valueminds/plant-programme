<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>PLANT | <?php echo is_single() ? get_the_title() : ""; ?> <?php echo is_page() ? rwmb_meta( 'meta_title' ) : ""; ?> </title>
	
	<meta property="og:title" content="PLANT | <?php the_title() ?>">
	<meta property="og:description" content="<?php echo is_single() ? get_the_title() : ""; ?> <?php echo is_page() ? rwmb_meta( 'meta_title' ) : ""; ?>">
	<meta property="og:image" content="<?php theme_dir() ?>/img/logo.jpg">
	<meta property="og:url" content="<?php the_permalink() ?>">
	<meta name="twitter:card" content="summary_large_image">

	<meta property="og:site_name" content="PLANT">
	<meta name="twitter:image:alt" content="PLANT logo">
	
	<link rel="shortcut icon" href="img/favicon.png" type="image/png">
	<link rel="icon" href="<?php theme_dir() ?>/img/favicon.png" type="image/png">
	<link href="https://fonts.googleapis.com/css?family=Slabo+13px|Source+Sans+Pro&display=swap" rel="stylesheet">
	<!--Load Styles-->
	<?php wp_head(); ?>
	<script src="<?php theme_dir() ?>/js/modernizr.js"></script> <!-- Modernizr -->
	<style>
		.filterBar {
			padding: 15px 0;
		}
		
		.tile_image_wrapper{
			background-position: center !important;
			background-size: cover !important;
			background-repeat: no-repeat !important;
		}
		
		.pager_nav .pager_item .current{
			display: block;
			padding: 4.5px 12px;
			border-radius: 4px;
			text-transform: capitalize;
			color: #050731;
			
			pointer-events: none;
			font-weight: 700;
			background-color: #d5d5d5;
		}
	</style>
</head>

<body>
	<header role="banner" class="preAuthHeader">
		<div class="headerStrips">
			<span class="headerStripBar bg__red"></span>
			<span class="headerStripBar bg__secondary"></span>
			<span class="headerStripBar bg__primary"></span>
			<span class="headerStripBar bg__inverse"></span>
		</div>
		<div class="container">
			<div class="navbarWrapper">
				<div class="navbarGroupItem navBarItemLeft">
					<div class="brand_img">
						<a href="<?php echo get_site_url(); ?>" class="logo_link">
							<img src="<?php theme_dir() ?>/img/logo.png" alt="Plant">
						</a>
					</div>
				</div>
				<div class="navbarGroupItem navBarItemRight">
					<nav class="navMenu menuPrimary">
						<div class="menuListItem">
							<a href="<?php echo get_site_url(); ?>/about" class="menuLinkItem">About</a>
						</div>
						<!-- navMenuActive -->
						<div class="menuListItem hasChildren">
							<a href="#" class="menuLinkItem">Interventions</a>
							<div class="subNav">
								<div class="container">
									<div class="row">
										<div class="subNavCol subNavHeader col-md-4">
											<h3 class="title title_sm color__white font-bold margin_bottom_sm">Interventions</h3>
											<p>Explore and join any of our Agtech ecosystem development programmes</p>
											<div class="margin_top_sm">
												<a href="<?php echo get_site_url(); ?>/apply" class="btn btn-primary">Apply Now</a>
											</div>
										</div>
										<div class="subNavCol subNavCol__navs col-md-8">
											<ul class="row">
												<?php 
													$args = array(
														'post_type' => 'intervention',
														'post_status' => 'publish',
														'posts_per_page' => -1,
													);
													$custom_posts = new WP_Query( $args );
												?>
												<?php if ( $custom_posts->have_posts() ) : ?> 
													<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
														<li class="subNavListItem col-md-6">
															<a href="<?php the_permalink() ?>" class="subNavLink"><?php the_title() ?></a>
														</li>
													<?php endwhile; ?>
												<?php endif; ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="menuListItem">
							<a href="<?php echo get_site_url(); ?>/ain" class="menuLinkItem">AIN</a>
						</div>
						<div class="menuListItem hasChildren">
							<a href="#" class="menuLinkItem">Engage</a>
							<div class="subNav">
								<div class="container">
									<div class="row">
										<div class="subNavCol subNavHeader col-md-4">
											<h3 class="title title_sm color__white font-bold margin_bottom_sm">Engage</h3>
											<p>Find valuable Agtech resources, explore FAQs, jobs and tender information across the Agtech Interface Network</p>
										</div>
										<div class="subNavCol subNavCol__navs col-md-8">
											<ul class="subNavList">
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/apply" class="subNavLink">Apply</a></li>
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/tenders" class="subNavLink">Tenders</a></li>
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/jobs" class="subNavLink">Jobs</a></li>
											</ul>
											<ul class="subNavList">
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/downloads" class="subNavLink">Downloads</a></li>
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/faqs" class="subNavLink">FAQs</a></li>
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/contact-us" class="subNavLink">Contact</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="menuListItem hasChildren">
							<a href="#" class="menuLinkItem">Media Center</a>
							<div class="subNav">
								<div class="container">
									<div class="row">
										<div class="subNavCol subNavHeader col-md-4">
											<h3 class="title title_sm color__white font-bold margin_bottom_sm">Media Center</h3>
											<p>Explore events, news, updates, opinions and ideas across the Agtech Ecosystem</p>
										</div>
										<div class="subNavCol subNavCol__navs col-md-8">
											<ul class="subNavList">
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/events" class="subNavLink">Events</a></li>
												<li class="subNavListItem"><a href="<?php echo get_site_url(); ?>/news-and-updates" class="subNavLink">News &amp; Updates</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="menuListItem btn_user_act">
							<a class="btn btn-primary-outline" href="<?php echo get_site_url(); ?>/apply">Apply Now</a>
						</div>
					</nav>
					<nav class="navMenu menuAlt">
						<div class="controlBtn nav_trigger">
							<a class="primary-nav-trigger" href="#">
								<span class="menu-icon"></span>
								<span class="menu-text">Menu</span>
							</a>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
