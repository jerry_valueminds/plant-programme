<?php /*Template Name: Tenders*/ ?>
<?php get_header() ?>
<?php while ( have_posts() ) : the_post(); ?>
<main class="mainContentArea bg__off_white ">
	<section class="section_block bg__white">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 heroContentBox">
					<h2 class="title title_md font-bold color__red margin_bottom_sm">Tenders</h2>
					<p class="font-md color__inverse"><?php echo rwmb_meta( 'page_excerpt' ) ?></p>
				</div>
			</div>
		</div>
	</section>
	<?php 
		$terms_array = array(
			array(
				'title'	=>	'Type',
				'slug'	=>	'tender-type'
			),
			array(
				'title'	=>	'Location',
				'slug'	=>	'location'
			),
		) 
	?>
	<?php include( locate_template( 'template_parts/post_filter.php', false, false ) ); ?>
	<section class="section_block ">
		<div class="container">
			<?php 
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$endOfDay   = strtotime("yesterday");
				$args = array(
					'post_type' => 'tender',
					'post_status' => 'publish',
					'posts_per_page' => 10,
					'paged' => $paged,
					'tax_query' => get_tax_array(),
					'meta_query' => array(
						array(
							'key'     => 'expiry_date',
							'value'   => $endOfDay,
							'compare' => '>',
						),
					)
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
			<div class="tenderList">
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<?php include( locate_template( 'template_parts/_tender.php', false, false ) ); ?>
				<?php endwhile; ?>
			</div>
			<div class="section_cto pager text-center">
				<nav class="pager_nav">
					<?php numbered_pagination( $custom_posts ); ?>
				</nav>
			</div>
			<?php else : ?>
				<p class="title font-bold">There are no tenders available at the moment.</p>
			<?php endif; ?>
		</div>
	</section>
</main>
<?php endwhile; ?>


<?php get_footer() ?>