<?php
    
/* 
*
*===================================================================================================================
* Enqueue Front Styles & Scripts BEGIN
*===================================================================================================================
*
*/
function saedconnect_script_enqueue(){
	//Enqueue Styles
	wp_enqueue_style('Reset', get_template_directory_uri().'/css/reset.css', array(), '4.0.0', 'all');
	wp_enqueue_style('Base', get_template_directory_uri().'/css/base.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style('Styles', get_template_directory_uri().'/css/styles.css', array(), '1.0.0', 'all');

	//Enqueue Scripts
	wp_enqueue_script('PopperJs', get_template_directory_uri().'/js/jquery.js', array(), '4.0.0', true);
	wp_enqueue_script('JQuery', get_template_directory_uri().'/js/base.min.js"', array('jquery'), '3.0.0', true);
	wp_enqueue_script('BootstrapJs', get_template_directory_uri().'/js/main.js', array(), '4.0.0', true);
}

//Call function on wp_enqueue_scripts
add_action('wp_enqueue_scripts', 'saedconnect_script_enqueue');


/* 
*
*===================================================================================================================
* Remove Extra Space on top of pages when admin is logged in
*===================================================================================================================
*
*/
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}


/* 
*
*===================================================================================================================
* Activate WP Theme Support BEGIN
*===================================================================================================================
*
*/

function custom_theme_setup(){
	//Activate Post Thumbnails
	add_theme_support('post-thumbnails');

	//Activate Menu Support
	add_theme_support('menus');

	//Activate Post Formats
	add_theme_support('post-formats', array('aside', 'image', 'video'));

	//Activate Custom Menu Support
	register_nav_menu('primary', 'Main Navigation Menu');
}
add_action('init', 'custom_theme_setup');


function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');



/* 
*
*===================================================================================================================
* ADD Theme Settings Page
*===================================================================================================================
*
*/
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
	return $content;
});


/* 
*
*===================================================================================================================
* Include Meta Box Files BEGIN
*===================================================================================================================
*
*/
include 'includes/metabox_files/post_relationships.php';
include 'includes/metabox_files/settings.php';


/* 
*
*===================================================================================================================
* Default Params
*===================================================================================================================
*
*/
$feat_updates_params = array( 'parent' => 0, 'relationship' => '', 'title' => 'News & Updates', 'button_alignment' => 'center' );


/* 
*
*===================================================================================================================
* Helper Functions
*===================================================================================================================
*
*/

function theme_dir(){
	echo get_stylesheet_directory_uri();
}

function currentUrl( $trim_query_string = false ) {
	$pageURL = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
	$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
	
	if( ! $trim_query_string ) {
		return $pageURL;
	} else {
		$url = explode( '?', $pageURL );
		return $url[0];
	}
}

function truncate($string, $length){
	if (strlen($string) > $length) {
		$string = substr($string, 0, $length) . '...';
	}

	return $string;
}

function get_metabox_image_url($key){
	/* Reset */
	$image = 0;
	$images = rwmb_meta( $key, array( 'limit' => 1 ) );
  $image = reset( $images );
	
	return $image['full_url'];
}

function get_metabox_image_alt($key){
	/* Reset */
	$image = 0;
	$images = rwmb_meta( $key, array( 'limit' => 1 ) );
  $image = reset( $images );
	
	return $image['alt'];
}

function get_metabox_group_image_url( $array, $key ){
	/* Reset */
	$image = 0;
	$image_ids = isset( $array[$key] ) ? $array[$key] : array();

	foreach ( $image_ids as $image_id ) {
		$image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
	}
	
	return $image['full_url'];
}

function get_metabox_group_image_alt( $array, $key ){
	/* Reset */
	$image = 0;
	$image_ids = isset( $array[$key] ) ? $array[$key] : array();

	foreach ( $image_ids as $image_id ) {
		$image = RWMB_Image_Field::file_info( $image_id, array( 'size' => 'thumbnail' ) );
	}
	
	return $image['alt'];
}

function get_first_term( $term_slug ){
	$term_list = wp_get_post_terms( get_the_ID(), $term_slug, array( 'fields' => 'names' ) );
	return $term_list[0];
}

function get_tax_array(){
	$tax_array = array();

	foreach( $_GET as $key => $value ){
		if($value){
			$tax_entry = array(
				array(
					'taxonomy' => $key,
					'field'    => 'slug',
					'terms'    => $value,
				)
			);
			$tax_array = array_merge($tax_array, $tax_entry);
		}
	}
	
	return $tax_array;
}

function list_terms( $term_list ){
	if ( $term_list[0] ){
		$count = count($term_list);

		foreach ( $term_list as $key => $term_item ){
			if( 
				$count > 1 
				&& $key > 0 
				&& $key < ($count - 1)
			){
				echo ', '.$term_item;
			}elseif( $count > 1 && $key == ($count - 1) ){
				echo ' & '.$term_item;
			}else{
				echo $term_item;
			}
		}
	} 
}

function pagination_bar( $query_wp ) {
	$pages = $query_wp->max_num_pages;
	$big = 999999999; // need an unlikely integer
	if ($pages > 1)
	{
		$page_current = max(1, get_query_var('paged'));
		echo paginate_links(array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => $page_current,
				'total' => $pages,
		));
	}
}

function numbered_pagination($query_wp) {
	if ( $query_wp->max_num_pages <= 1 ) return; 

	$big = 999999999; // need an unlikely integer

	$pages = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $query_wp->max_num_pages,
		'type'  => 'array',
		'prev_text' => '<span>Previous</span>',
  	'next_text' => '<span>Next</span>'
	) );
	
	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');

		foreach ( $pages as $page ) {
			echo '<div class="pager_item">'.$page.'</div>';
		}
	}
}