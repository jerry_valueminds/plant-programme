<?php /*Template Name: Apply*/ ?>
<?php get_header() ?>
<?php while ( have_posts() ) : the_post(); ?>
<main class="mainContentArea bg__off_white ">
	<section class="section_block bg__white">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 heroContentBox">
					<h2 class="title title_md font-bold color__inverse margin_bottom_sm">Apply</h2>
					<p class="font-md color__inverse"><?php echo rwmb_meta( 'page_excerpt' ) ?></p>
				</div>
			</div>
		</div>
	</section>
	<section class="section_block bg__primary">
		<div class="container ">
			<div class="interventionList">
			<?php 
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$endOfDay   = strtotime("yesterday");
				$args = array(
					'post_type' => 'cohort',
					'post_status' => 'publish',
					'posts_per_page' => 6,
					'paged' => $paged,
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key'     => 'always_available',
							'value'   => '1',
							'compare' => '==',
						),
						array(
							'key'     => 'expiry_date',
							'value'   => $endOfDay,
							'compare' => '>',
						),
					)
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<?php include( locate_template( 'template_parts/_cohort.php', false, false ) ); ?>
				<?php endwhile; ?>
				<div class="section_cto pager text-center">
					<nav class="pager_nav">
						<?php numbered_pagination( $custom_posts ); ?>
					</nav>
				</div>
			<?php else : ?>
				<p class="title font-bold color__white">There are no applications available at the moment.</p>
			<?php endif; ?>
			</div>
		</div>
	</section>
</main>
<?php endwhile; ?>
<?php get_footer() ?>
