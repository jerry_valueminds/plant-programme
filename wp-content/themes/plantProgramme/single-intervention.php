<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php
	$parent_id = get_the_ID();
	$author_id = get_post_field( 'post_author', get_the_ID() );
	$term_list = wp_get_post_terms( $post->ID, 'category', array( 'fields' => 'slugs' ) );
	$intervention_title = get_the_title();
?>

<main class="mainContentArea bg__off_white">
	<section class="section_block bg__white">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-5 heroContentBox ">
					<span class="headerSupport font-bold color__primary caps_upper">INTERVENTION</span>
					<h2 class="title title_md font-bold margin_bottom_sm" style="color: <?php echo rwmb_meta( 'color' ) ?> !important;"><?php the_title() ?></h2>
					<p class="font-md"><?php echo rwmb_meta( 'highlight_content' ) ?></p>
					<div class="section_cto">
						<a href="#cohort_details" class="btn btn-primary" target="_blank">Apply Now</a>
					</div>
				</div>
				<div class="offset-md-1 col-md-6 image_stub">
					<img src="<?php echo get_metabox_image_url('picture') ?>" alt="<?php echo get_metabox_image_alt('picture') ?>">
				</div>
			</div>
		</div>
	</section>
	<div class="headerStrips baseStrip">
		<span class="headerStripBar bg__red"></span>
		<span class="headerStripBar bg__secondary"></span>
		<span class="headerStripBar bg__primary"></span>
		<span class="headerStripBar bg__inverse"></span>
	</div>
	<section class="section_block padding_bottom_none">
		<div class="container ">
			<div class="margin_bottom_xl">
				<h2 class="title font-bold margin_bottom_md"><?php echo rwmb_meta( 'summary_title' ) ?></h2>
				<p><?php echo rwmb_meta( 'summary_content' ) ?></p>
			</div>
		</div>
	</section>
	<section class="section_block padding-top-none">
		<div class="bannerBlobItem homeBlobCenterLeft"></div>
		<div class="container">
			<div class="row color_text_alt">
				<div class="col-md-6">
					<header class="section_header">
						<h2 class="title title_sm font-bold color__primary margin_bottom_md margin_top_sm">The <?php the_title() ?> Program is packed full with benefits</h2>
					</header>
				</div>
			</div>
			<?php $benefits = rwmb_meta('benefits'); ?>
			<div class="row ">
				<?php foreach( $benefits['benefit'] as $benefit ){ ?>
				<article class="col-md-4 featureListItem">
					<div class="featureCardWrapper">
						<div class="margin_bottom_md cardFigureIcon">
							<img src="<?php echo get_metabox_group_image_url( $benefits, 'benefits_icon' ); ?>" alt="Discover" class="">
						</div>
						<p><?php echo $benefit ?></p>
					</div>
				</article>
				<?php } ?>
			</div>
		</div>
	</section>
	<section class="section_block bg__secondary" id="cohort_details" style="background-color: <?php echo rwmb_meta( 'color' ) ?> !important;">
		<div class="container ">

			<div class="row color__white">
				<div class="offset-md-1 col-md-8">
					<header class="section_header">
						<h2 class="title title_sm font-bold margin_bottom_sm">Who can Apply?</h2>
						<p><?php echo rwmb_meta( 'cohort_summary' ) ?></p>
					</header>
				</div>
			</div>
			<?php 
				$endOfDay   = strtotime("yesterday");
				$args = array(
					'post_type' => 'cohort',
					'post_status' => 'publish',
					'posts_per_page' => 1,
					'relationship' => array(
						'id'   => 'intervention_to_cohort',
						'from' => $parent_id
					),
					'meta_query' => array(
						array(
							'key'     => 'expiry_date',
							'value'   => $endOfDay,
							'compare' => '>',
						),
					)
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<?php include( locate_template( 'template_parts/_cohort.php', false, false ) ); ?>
				<?php endwhile; ?>
			<?php else : ?>
				<section class="tile_block bg__white">
					<div class="tile_item text-center">
						<div class="tile_content_wrapper bg-off-white">
							<div class="content_inner">
								<h3 class="title title_sm color__primary font-bold">Date of Next Cohort will be announced soon</h3>
								<div class="eventDescription margin_top_sm">
									<p class="font-lg">Join the Agtech Interface Network to get notified <br /> when the next cohort opens </p>
								</div>
								<div class="margin_top_md">
									<a href="<?php echo get_site_url(); ?>/apply" class="btn btn-primary">Sign Up</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			<?php endif; ?>
		</div>
	</section>
	<section class="section_block bg__white">
		<div class="container content_narrow">
			<header class="margin_bottom_xl">
				<p class="color__grey_dark">Explore our Frequently Asked Questions </p>
				<h3 class="title title_md font-bold color__primary">Frequently Asked Questions</h3>
			</header>
			<?php 
				$args = array(
					'post_type' => 'faq',
					'post_status' => 'publish',
					'posts_per_page' => 6,
					'relationship' => array(
						'id'   => 'intervention_to_faq',
						'from' => $parent_id
					)
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
			<div class="row">
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<article class="col-md-6 featureListItem color__grey_dark">
					<h4 class="title title_sm font-bold margin_bottom_sm color__black"><?php echo rwmb_meta( 'question' ) ?></h4>
					<p><?php echo rwmb_meta( 'answer' ) ?></p>
				</article>
				<?php endwhile; ?>
			</div>
			<div class="section_cto">
				<a href="<?php echo get_site_url(); ?>/faqs" class="btn btn-primary">
					See More
				</a>
			</div>
			<?php else: ?>
			<p class="title font-bold">There are no FAQs available at the moment.</p>
			<?php endif; ?>
		</div>
	</section>
	
	<?php 
		$feat_updates_params = array( 
			'parent' 						=> $parent_id, 
			'relationship'			=> 'intervention_to_update', 
			'title'							=> ' Updates on '.$intervention_title, 
			'button_alignment'	=> 'left' 
		); 
	?>
	<?php include( locate_template( 'template_parts/updates.php', false, false ) ); ?>
</main>

<?php endwhile; ?> 

<?php get_footer() ?>
