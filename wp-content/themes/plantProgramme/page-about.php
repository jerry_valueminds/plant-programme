<?php /*Template Name: About*/ ?>
<?php get_header() ?>
<?php 
	while ( have_posts() ) : the_post();
		$section_one_data = rwmb_meta('about_page_section_one');
		$section_two_data = rwmb_meta('about_page_section_two');
		$section_three_data = rwmb_meta('about_page_section_three');
	endwhile; 
?>

<?php if( count( $section_three_data['section_three_highlights'] ) ){ ?>
	<?php foreach( $section_three_data['section_three_highlights'] as $key => $list_item ){ ?>
		<div class="modal fade" id="post_<?php echo $key ?>">
			<div class="modal-dialog modal-lg postDialog">
				<div class="modal-content">
					<header class="modal-header">
						<div class="dialogDismiss">
							<button type="button" class="close_dialog" data-dismiss="modal"></button>
						</div>
					</header>
					<div class="modal-body">
						<div class="content_panel">
							<div class="section_header">
								<h2 class="title title_md font-bold color__primary"><?php echo $list_item['highlight_title']; ?></h2>
							</div>
							
							<?php 
								/* Reset */
								$image = 0;
								$image_ids = isset( $list_item['highlight_picture'] ) ? $list_item['highlight_picture'] : array();				 
							?>
							<?php if( count( $image_ids ) <= 1 ){ ?>
								<div class="featuredImage">
									<figure>
										<img src="<?php echo get_metabox_group_image_url( $list_item, 'highlight_picture' ); ?>" alt="<?php echo get_metabox_group_image_alt( $list_item, 'highlight_picture' ); ?>">
									</figure>
								</div>
							<?php }else{ ?>
								<div class="pictureTiles bg__white featuredImage">
									<div class="row">
										<article class="galleryThumbGroup col-md-4">
											<div class="galleryThumbWrapper">
												<div class="galleryImageHolder">
												<?php if( $image_ids[0] ) ?>
													<?php $image = RWMB_Image_Field::file_info( $image_ids[0], array( 'size' => 'thumbnail' ) ); ?>
													<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
												<?php ?>
												</div>
											</div>
										</article>
										<article class="galleryThumbGroup col-md-4">
											<div class="galleryThumbWrapper gThumbvHalf">
												<div class="galleryImageHolder">
												<?php if( $image_ids[1] ) ?>
													<?php $image = RWMB_Image_Field::file_info( $image_ids[1], array( 'size' => 'thumbnail' ) ); ?>
													<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
												<?php ?>
												</div>
												<div class="galleryImageHolder">
												<?php if( $image_ids[2] ) ?>
													<?php $image = RWMB_Image_Field::file_info( $image_ids[2], array( 'size' => 'thumbnail' ) ); ?>
													<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
												<?php ?>
												</div>
											</div>
										</article>
										<article class="galleryThumbGroup col-md-4">
											<div class="galleryThumbWrapper">
												<div class="galleryImageHolder">
												<?php if( $image_ids[3] ) ?>
													<?php $image = RWMB_Image_Field::file_info( $image_ids[3], array( 'size' => 'thumbnail' ) ); ?>
													<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
												<?php ?>
												</div>
											</div>
										</article>
									</div>
								</div>
							<?php } ?>
							<div class="container blogLayoutWrapper">
								<div class="postArticle">
									<?php echo $list_item['highlight_content']; ?>
								</div>
								<div class="section_cto">
									<a href="#" data-dismiss="modal" class="btn btn-primary">Close</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>



<div class="modal fade" id="post_2">
	<div class="modal-dialog modal-lg postDialog">
		<div class="modal-content">
			<header class="modal-header">
				<div class="dialogDismiss">
					<button type="button" class="close_dialog" data-dismiss="modal"></button>
				</div>
			</header>
			<div class="modal-body">
				<div class="content_panel">
					<div class="section_header">
						<h2 class="title title_lg font-bold color__primary">Technology Holds a Huge Potential to Transform the Agricultural Landscape</h2>
					</div>
					<div class="pictureTiles bg__white featuredImage">
						<div class="row">
							<article class="galleryThumbGroup col-md-4">
								<div class="galleryThumbWrapper">
									<div class="galleryImageHolder">
										<img src="<?php theme_dir() ?>/img/banners/tileImg_1.jpg" alt="" title="Gallery image 1" />
									</div>
								</div>
							</article>
							<article class="galleryThumbGroup col-md-4">
								<div class="galleryThumbWrapper gThumbvHalf">
									<div class="galleryImageHolder">
										<img src="<?php theme_dir() ?>/img/banners/tileImg_2.jpg" alt="" title="Gallery image 2" />
									</div>
									<div class="galleryImageHolder">
										<img src="<?php theme_dir() ?>/img/banners/tileImg_3.jpg" alt="" title="Gallery image 3" />
									</div>
								</div>
							</article>
							<article class="galleryThumbGroup col-md-4">
								<div class="galleryThumbWrapper">
									<div class="galleryImageHolder">
										<img src="<?php theme_dir() ?>/img/banners/tileImg_4.jpg" alt="" title="Gallery image 4" />
									</div>
								</div>
							</article>
						</div>
					</div>
					<div class="container blogLayoutWrapper">
						<div class="postArticle">
							<p>This current landscape of the Agricultural sector may seem like a bleak picture, but for the first time, recent technological and connectivity innovations and developments, combined with new pathways for accessing investment and financing, and new business modelling approaches, hold real potential to either remove, ease or simply bypass many of these constraints at a truly transformation scale.</p>
							<p>In addition, access to new generations of seeds and ancillary products, have the potential to reduce food security, and increase productivity, quality and nutrition. There are a handful of emerging agricultural technology applications changing the face of the sector, including solutions that does the following, amongst others”</p>
							<ul>
								<li>Enable widespread access to farm mechanization coordinated through online platforms.</li>
								<li>Produce and input marketing and supply / distribution logistics through online platforms and E-commerce.</li>
								<li>Staple crops biofortified with increased vitamins and minerals.</li>
								<li>Renewable power solutions for storage, irrigation and primary processing.</li>
								<li>Small scale business models replicating a proven model many times over, using technology to monitor and coordinate the networks, including procurement and sales.</li>
								<li>Technology enabled new forms of investment such as crowd funding.</li>
								<li>Revenue generating (and therefore self-sustaining) mobile phone based agricultural information exchange platforms.</li>
								<li>Resource mapping and weather sensing technology.</li>
								<li>Land and resource rights and ownership validating technologies.</li>
								<li>Value chain trust brokering and inter-firm functions including blockchain, supply chain coordination and mobile money non-cash (low risk) financial flow mechanisms.</li>
								<li>Efficient resource directing innovations such as AI driven satellite imagery analysis.</li>
								<li>Financial performance track record systems to unlock finance and investment.</li>
								<li>Revolutions in local level parts and equipment manufacture through 3d printing.</li>
							</ul>

							<p>Many of these innovations can finally help to solve two key challenges that have afflicted agricultural development efforts for decades, namely reducing the transaction cost of doing business with millions of small, scattered, poor farmers, and developing profitable, self-sustaining and incentivised business models that recognize the poor as a market in a beneficial but not exploitative way.</p>
						</div>
						<div class="section_cto">
							<a href="#" data-dismiss="modal" class="btn btn-primary">Close</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<main class="mainContentArea bg__off_white">
	<section class="section_block bg__white ">
		<div class="bannerBlobItem aboutBlobTopLeft"></div>
		<div class="bannerBlobItem aboutBlobTopright"></div>
		<div class="container">
			<div class="row">
				<div class="offset-md-2 col-md-8 text-center">
					<div class="margin_bottom_sm">
						<img src="<?php echo get_metabox_group_image_url( $section_one_data, 'page_icon' ); ?>" alt="<?php echo get_metabox_group_image_alt( $section_one_data, 'page_icon' ); ?>" class="featureIcon">
					</div>
					<h4 class="title title_lg color__primary font-bold margin_bottom_md"><?php echo $section_one_data['page_title']; ?></h4>
					<p class="font-bold font-lg"><?php echo $section_one_data['page_summary']; ?></p>
				</div>
			</div>
		</div>
	</section>
	<div class="pictureTiles bg__white">
		<?php 
			/* Reset */
			$image = 0;
			$image_ids = isset( $section_one_data['page_heroes'] ) ? $section_one_data['page_heroes'] : array();
		?>
		<div class="row">
			<article class="galleryThumbGroup col-md-4">
				<div class="galleryThumbWrapper">
					<div class="galleryImageHolder">
					<?php if( $image_ids[0] ) ?>
						<?php $image = RWMB_Image_Field::file_info( $image_ids[0], array( 'size' => 'thumbnail' ) ); ?>
						<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
					<?php ?>
					</div>
				</div>
			</article>
			<article class="galleryThumbGroup col-md-4">
				<div class="galleryThumbWrapper gThumbvHalf">
					<div class="galleryImageHolder">
					<?php if( $image_ids[1] ) ?>
						<?php $image = RWMB_Image_Field::file_info( $image_ids[1], array( 'size' => 'thumbnail' ) ); ?>
						<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
					<?php ?>
					</div>
					<div class="galleryImageHolder">
					<?php if( $image_ids[2] ) ?>
						<?php $image = RWMB_Image_Field::file_info( $image_ids[2], array( 'size' => 'thumbnail' ) ); ?>
						<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
					<?php ?>
					</div>
				</div>
			</article>
			<article class="galleryThumbGroup col-md-4">
				<div class="galleryThumbWrapper">
					<div class="galleryImageHolder">
					<?php if( $image_ids[3] ) ?>
						<?php $image = RWMB_Image_Field::file_info( $image_ids[3], array( 'size' => 'thumbnail' ) ); ?>
						<img src="<?php echo $image['full_url'] ?>" alt="" title="<?php echo $image['alt'] ?>" />
					<?php ?>
					</div>
				</div>
			</article>
		</div>
	</div>
	<section class="section_block bg__primary">
		<div class="container ">
			<div class="row text-center">
				<div class="offset-md-3 col-md-6">
					<header class="section_header">
						<span class="headerSupport color__white caps_upper"><?php echo $section_three_data['section_three_subtitle']; ?></span>
						<h2 class="title title_lg font-bold color__white margin_bottom_md"><?php echo $section_three_data['section_three_title']; ?></h2>
					</header>
				</div>
			</div>
			<?php if( count( $section_three_data['section_three_highlights'] ) ){ ?>
				<div class="row">
				<?php foreach( $section_three_data['section_three_highlights'] as $key => $list_item ){ ?>
					<article class="col-md-6 featureListItem color__white text-center-sm">
						<div class="margin_bottom_md">							
							<img src="<?php echo get_metabox_group_image_url( $list_item, 'highlight_icon' ); ?>" alt="<?php echo get_metabox_group_image_alt( $list_item, 'highlight_icon' ); ?>" class="featureIcon">
						</div>
						<h4 class="title title_sm color__white font-bold margin_bottom_lg"><?php echo $list_item['highlight_title']; ?></h4>
						<p><?php echo $list_item['highlight_summary']; ?></p>
						<div class="section_cto">
							<a href="#post_<?php echo $key ?>" data-toggle="modal" data-backdrop="static" class="btn-link color__white">
								<span class="btnLabel">Learn More</span>
								<span class="btnIcon">
									+
								</span>
							</a>
						</div>
					</article>
				<?php } ?>
				</div>
			<?php } ?>
		</div>
	</section>
	<section class="section_block padding_bottom_none">
		<div class="bannerBlobItem aboutBlobbottomRight"></div>
		<div class="container content_narrow">
			<div class="row text-center color_text_alt">
				<div class="offset-md-1 col-md-10">
					<header class="margin_bottom_xl">
						<h3 class="title title_sm font-bold color__black">The PLANT Programme is enabling the discovery and adoption of technology solutions that address real agribusiness constraints at the right cost to agribusinesses and at the appropriate sophistication level.</h3>
					</header>
				</div>
			</div>
			<?php if( count( $section_two_data['list'] ) ){ ?>
				<div class="row">
				<?php foreach( $section_two_data['list'] as $list_item ){ ?>
					<article class="col-md-4 featureListItem color__grey_dark text-center-sm">
						<header class="margin_bottom_md">
							<?php if( get_metabox_group_image_url( $list_item, 'list_item_picture' ) ){ ?>
								<img src="<?php echo get_metabox_group_image_url( $list_item, 'list_item_picture' ); ?>" alt="<?php echo get_metabox_group_image_alt( $list_item, 'list_item_picture' ); ?>" class="featureIcon">
							<?php } ?>
						</header>
						<p><?php echo $list_item['list_item_content']; ?></p>
					</article>
				<?php } ?>
				</div>
			<?php } ?>

			<div class="row text-center">
				<div class="offset-md-2 col-md-8">
					<div class="secDivider dividerMidi"></div>
					<p class="color__grey_dark"><?php echo $section_two_data['section_two_footer']; ?></p>
					<div class="secDivider dividerMini"></div>
				</div>
			</div>

			<div class="section_cto text-center">
				<a href="#interventionPanel" data-toggle="collapse" class="btn btn-primary">
					Explore Our Interventions
				</a>
			</div>
		</div>
	</section>
	<div class="collapse in" id="interventionPanel">
		<div class="section_block bg__primary">
			<div class="container content_narrow color__white">
			<?php 
				$args = array(
					'post_type' => 'intervention',
					'post_status' => 'publish',
					'posts_per_page' => -1,
				);
				$custom_posts = new WP_Query( $args );
			?>

			<?php if ( $custom_posts->have_posts() ) : ?> 
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<article class="interventionRow text-center">
					<h4 class="title title_sm font-bold color__white margin_bottom_sm">
						<a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
					<p><?php echo rwmb_meta( 'highlight_content' ) ?></p>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>
	<section class="section_block ">
		<div class="container ">
			<div class="row text-center color_text_alt">
				<div class="offset-md-3 col-md-6">
					<header class="section_header text-center">
						<span class="headerSupport color__grey_dark caps_upper">Who we are</span>
						<h2 class="title title_lg font-bold color__primary margin_bottom_sm">Team Members</h2>
						<p>The PLANT Programme is driven by a dynamic team of experts committed to increasing technology adoption to transform Nigeria’s Agricultural sector.</p>
					</header>
				</div>
			</div>

			<div class="row teamMemberGrid">
			<?php 
				$args = array(
					'post_type' => 'team-member',
					'post_status' => 'publish',
					'posts_per_page' => -1,
				);
				$custom_posts = new WP_Query( $args );
			?>

			<?php if ( $custom_posts->have_posts() ) : ?> 
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<article class="teamMemberItem col-6 col-sm-4">
						<div class="teamMemberBlock">
							<div class="memberImage">
								<img src="<?php echo get_metabox_image_url('picture') ?>" class="bannerImage" alt="<?php echo get_metabox_image_alt('picture') ?>">
							</div>
							<div class="memberDetails">
								<div class="memberInfo">
									<h4 class="title"><?php the_title() ?></h4>
									<p class="member_role"><?php echo rwmb_meta( 'designation' ) ?></p>
								</div>
								<div class="memberContact">
									<a href="<?php echo rwmb_meta( 'linkedin_url' ) ?>" target="_blank">
										<svg class="cust_icon icon_xs" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 58.47 55.88">
											<path d="M58.47,34.26V55.88H45.94V35.7c0-5.07-1.81-8.52-6.35-8.52a6.86,6.86,0,0,0-6.43,4.58,8.58,8.58,0,0,0-.42,3.06V55.88H20.21s.17-34.16,0-37.7H32.74v5.34l-.08.12h.08v-.12A12.45,12.45,0,0,1,44,17.29C52.29,17.29,58.47,22.68,58.47,34.26ZM7.09,0C2.81,0,0,2.81,0,6.51S2.72,13,6.93,13H7c4.37,0,7.09-2.9,7.09-6.52S11.38,0,7.09,0ZM.74,55.88H13.28V18.17H.74Z" />
										</svg>
									</a>
								</div>
							</div>
						</div>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<div class="text-center">
				<?php include( locate_template( 'template_parts/sponsors.php', false, false ) ); ?>
			</div>
		</div>
	</section>
</main>

<?php get_footer() ?>
