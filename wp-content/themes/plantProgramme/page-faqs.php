<?php /*Template Name: FAQs*/ ?>
<?php get_header() ?>
<?php 
	while ( have_posts() ) : the_post();
		
	endwhile; 
?>

<main class="mainContentArea bg__off_white ">
	<section class="section_block bg__white">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 heroContentBox">
					<h2 class="title title_md font-bold color__inverse margin_bottom_sm">Frequently Asked Questions</h2>
					<p class="font-md color__inverse"><?php echo rwmb_meta( 'page_excerpt' ) ?></p>
				</div>
			</div>
		</div>
	</section>
	<section class="bg__primary">
		<div class="container ">
			<div class="splitLayout row">
				<aside class="navSidebar col-md-3">
					<div class="contentWrapper navWrapper">
						<div class="navHeader">
							<span class="caps_upper">Select a Topic</span>
						</div>
						<nav class="sideNavBlock">
							<ul class="navBlockList" id="help_nav">
								<li class="navBlockItem <?php echo ( $_GET['topic'] == '' && $_GET['interventions'] == '' ) ? 'active' : ''; ?>">
									<a href="<?php echo currentUrl(true); ?>" class="navBlockLink">About PLANT</a>
								</li>
								<li class="navBlockItem <?php echo ( $_GET['topic'] == 'ain' ) ? 'active' : ''; ?>">
									<a href="<?php echo currentUrl(true).'?topic=ain'; ?>" class="navBlockLink">About AIN</a>
								</li>
								<?php 
									$args = array(
										'post_type' => 'intervention',
										'post_status' => 'publish',
										'posts_per_page' => 3,
									);
									$custom_posts = new WP_Query( $args );
								?>
								<?php if ( $custom_posts->have_posts() ) : ?>
									<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
										<li class="navBlockItem <?php echo ( get_the_ID() == $_GET['interventions'] ) ? 'active' : ''; ?>">
											<a href="<?php echo currentUrl(true).'?interventions='.get_the_ID(); ?>" class="navBlockLink"><?php the_title() ?></a>
										</li>
									<?php endwhile; ?>
								<?php endif; ?>
							</ul>
						</nav>
					</div>
				</aside>
				<div class="contentArea col-md-9">
					<div class="contentWrapper ">
						<header class="section_header">
							<span class="headerSupport color__inverse caps_upper">Frequently Asked Questions about</span>
							<h2 class="title title_sm font-bold color__white">
								<?php 
									if($_GET['interventions']){
										$post = get_post($_GET['interventions']);
										echo $post->post_title;
									}elseif($_GET['topic'] == 'ain'){
										echo 'About AIN';
									}else{
										echo 'About PLANT';
									}
								?>
							</h2>
						</header>
						<div class="panel-group" id="questionPanel">
						<?php 
							$args = array(
								'post_type' => 'faq',
								'post_status' => 'publish',
								'posts_per_page' => -1,
							);
							
							if($_GET['topic']){
								
								$args['meta_query'] = array(
									array(
										'key'     => 'topics',
										'value'   => $_GET['topic'],
										'compare' => '==',
									),
								);
								
							} elseif ($_GET['interventions']){
								
								$args['relationship'] = array(
									'id'   => 'intervention_to_faq',
									'from' => $_GET['interventions'], // You can pass object ID or full object
								);	
								
							} else {
								
								$args['meta_query'] = array(
									array(
										'key'     => 'topics',
										'value'   => 'about',
										'compare' => '==',
									),
								);
								
							}

							$custom_posts = new WP_Query( $args );
						?>
						<?php if ( $custom_posts->have_posts() ) : ?> 
							<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
							<article class="panel panel-FAQs">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#questionPanel" href="#question_<?php echo get_the_ID(); ?>"><?php echo rwmb_meta( 'question' ) ?></a>
									</h4>
								</div>
								<div id="question_<?php echo get_the_ID(); ?>" class="panel-collapse collapse ">
									<div class="panel-body">
										<p><?php echo rwmb_meta( 'answer' ) ?></p>
									</div>
								</div>
							</article>
							<?php endwhile; ?>
						<?php else : ?>
							<p class="title font-bold color__white">There are no FAQs available at the moment.</p>
						<?php endif; ?>
						</div>
						<div class="section_cto margin_top_xl">
							<h6 class="title color__white margin_bottom_sm">Cant find the answer to your question?</h6>
							<a href="<?php echo get_site_url(); ?>/contact-us" class="btn btn-default-outline">Ask the team</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer() ?>
