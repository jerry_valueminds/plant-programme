<section class="tile_block bg__white rows no-gutters">
	<div class="tile_item tile_left col-md-7">
		<div class="tile_content_wrapper bg-off-white">
			<div class="content_inner contentCollapsed">
				<div class="eventCardTime color__grey_dark">
					<time>
						<?php 
							if(rwmb_meta( 'always_available' ) == '1' ){
								echo 'Receiving Applications';
							}else{
								$value = rwmb_meta( 'start_date' );
								echo date( 'jS F Y', $value );

								$value = rwmb_meta( 'end_date' );
								echo ' - '.date( 'jS F Y', $value );
							}
						?>
					</time>
				</div>
				<h3 class="title title_sm color__primary font-bold"><?php the_title() ?></h3>
				<div class="eventInfoList">
					<div class="infoListItem color__grey_dark">
						<span class="listItemLabel">Location:</span>
						<span class="listItemValue">
							<?php 
								$term_list = wp_get_post_terms( get_the_ID(), 'location', array( 'fields' => 'names' ) );
								list_terms( $term_list );
							?>						
						</span>
					</div>
				</div>
				<div class="eventDescription">
					<p><?php echo rwmb_meta( 'summary' ) ?></p>
					<p><?php echo rwmb_meta( 'content' ) ?></p>
				</div>
				<div class="section_cto">
					<a href="<?php echo rwmb_meta( 'cohort_application_link' ) ?>" target="_blank" class="btn btn-primary">Apply Now</a>
					<button type="button" class="btn-link height_toggle" id="show_more">
						<span class="btnLabel">Learn More</span>
						<span class="btnIcon">
							+
						</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="tile_item tile_right col-md-5">
		<div class="tile_image_wrapper" style="background-image:url('<?php echo get_metabox_image_url('cohort_picture') ?>')">
			<!--<img class="d-none" src="<?php echo get_metabox_image_url('cohort_picture') ?>" alt="<?php echo get_metabox_image_alt('cohort_picture') ?>">-->
		</div>
	</div>
</section>
