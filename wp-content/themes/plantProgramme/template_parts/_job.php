<article class="col-sm-6 col-md-4 cardItem">
	<a href="<?php the_permalink() ?>" class="cardWrapper jobCardWrapper">
		<div class="cardInfoPrimary">
			<div class="cardMetaRow">
				<?php if( get_first_term('job-type') ){ ?>
					<div class="metaItem">
						<span class="color__secondary"><?php echo get_first_term('job-type') ?></span>
					</div>
				<?php } ?>
				<?php if( rwmb_meta( 'expiry_date' ) ){ ?>
					<div class="metaItem">
						<span>
							<?php 
								$value = rwmb_meta( 'expiry_date' );
								echo 'Expires '.date( 'j-M-Y', $value );
							?>
						</span>
					</div>
				<?php } ?>
			</div>
			<h4 class="title title_sm font-bold"><?php the_title() ?></h4>
		</div>
		<?php if( get_first_term('location') ){ ?>
		<div class="cardinfoFooter">
			<div class="postMeta postMetaLocation">
				<span><?php echo get_first_term('location') ?></span>
			</div>
		</div>
		<?php } ?>
	</a>
</article>
