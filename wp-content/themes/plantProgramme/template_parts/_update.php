<article class="postThumb col-sm-6 col-md-4">
	<div class="postThumbWrapper">
		<div class="postThumbImage">
			<a href="<?php the_permalink() ?>" class="postImageWrapper">
				<img src="<?php echo get_metabox_image_url('picture') ?>" class="bannerImage" alt="<?php echo get_metabox_image_alt('picture') ?>">
			</a>
		</div>
		<div class="postThumbDetails">
			<?php if( get_first_term( 'blog-category' ) ){ ?>
				<div class="postCatHolder margin_bottom_md">
					<span class="postCatTag"><?php echo get_first_term( 'blog-category' ) ?></span>
				</div>
			<?php } ?>
			<h2 class="title title_sm font-bold margin_bottom_md"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
			<div class="postMetaHolder">
				<div class="postMeta postMetaDate">
					<span><?php echo get_the_date( 'F j, Y' ) ?></span>
				</div>
			</div>
		</div>
	</div>
</article>
