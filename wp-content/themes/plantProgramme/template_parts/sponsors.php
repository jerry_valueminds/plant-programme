<style>
	.logo_item img {
		width: auto;
	}
</style>
<?php 
	$args = array(
		'post_type' => 'partner',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key'     => 'type',
				'value'   => 'Implementing',
				'compare' => '==',
			),
		)
	);
	$custom_posts = new WP_Query( $args );
?>
  
<?php if ( $custom_posts->have_posts() ) : ?> 
	<div class="sponsorsWrapper">
		<div class="secDivider dividerMini"></div>
		<h4 class="title color__grey_dark">Implementing Partners</h4>
			<div class="logo_grid align-items-center">
			<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<div class="logo_item ">
					<img 
						src="<?php echo get_metabox_image_url('logo') ?>" 
						alt="<?php echo get_metabox_image_alt('logo') ?>" 
						style="height:<?php echo (rwmb_meta('size')) ? rwmb_meta('size') : '35px'; ?>"
					>
				</div>
			<?php endwhile; ?>
			</div>
	</div>
<?php endif; ?>

<?php 
	$args = array(
		'post_type' => 'partner',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key'     => 'type',
				'value'   => 'funding',
				'compare' => '==',
			),
		)
	);
	$custom_posts = new WP_Query( $args );
?>
  
<?php if ( $custom_posts->have_posts() ) : ?> 
	<div class="sponsorsWrapper">
		<div class="secDivider dividerMini"></div>
		<h4 class="title color__grey_dark">Funded by</h4>
			<div class="logo_grid align-items-center">
			<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<div class="logo_item ">
					<img 
						src="<?php echo get_metabox_image_url('logo') ?>" 
						alt="<?php echo get_metabox_image_alt('logo') ?>" 
						style="height:<?php echo (rwmb_meta('size')) ? rwmb_meta('size') : '35px'; ?>"
					>
				</div>
			<?php endwhile; ?>
			</div>
	</div>
<?php endif; ?>

<div class="section_cto text-center">
	<a href="<?php echo get_site_url(); ?>/about" class="btn-link">
		<span class="btnLabel">Learn More</span>
		<span class="btnIcon">
			<svg class="cust_icon" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
				<rect width="16" height="16" fill="white" fill-opacity="0.01"></rect>
				<path d="M10.5858 9.00001H2V7.00001H10.5858L7.29289 3.70712L8.70711 2.29291L14.4142 8.00001L8.70711 13.7071L7.29289 12.2929L10.5858 9.00001Z"></path>
			</svg>
		</span>
	</a>
</div>
