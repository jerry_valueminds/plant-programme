<article class="cardItem">
	<a href="<?php the_permalink() ?>" class="cardWrapper tenderCardWrapper">
		<div class="cardInfoPrimary">
			<?php $term_list = wp_get_post_terms( get_the_ID(), 'tender-type', array( 'fields' => 'names' ) ); ?>
			<?php if ( $term_list[0] ){ ?>
			<div class="cardMetaRow">
				<?php foreach ( $term_list as $term_item ){ ?>
					<div class="metaItem">
						<span class="color__primary text-uppercase"><?php echo $term_item ?></span>
					</div>
				<?php } ?>
			</div>
			<?php } ?>
			<div class="accordion-toggle collapsed">
				<h4 class="title title_sm color__red font-bold"><?php the_title() ?></h4>
			</div>
		</div>
		<div class="cardinfoFooter">
			<div class="metaItem">
				<span>
					<?php 
						$value = rwmb_meta( 'expiry_date' );
						echo 'Expires '.date( 'j-M-Y', $value );
					?>
				</span>
			</div>
		</div>
	</a>
</article>
