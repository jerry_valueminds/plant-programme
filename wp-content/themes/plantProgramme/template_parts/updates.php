<?php 
	$args = array(
		'post_type' => 'update',
		'post_status' => 'publish',
		'posts_per_page' => 3,
	);

	if( $feat_updates_params['relationship'] && $feat_updates_params['parent'] ){
		$args['relationship'] = array(
			'id'   => $feat_updates_params['relationship'],
			'from' => $feat_updates_params['parent'], // You can pass object ID or full object
    );
	}

	$custom_posts = new WP_Query( $args );
?>

<section class="section_block ">
	<div class="container ">
	<?php if ( $custom_posts->have_posts() ) : ?> 
		<header class="section_header text-center">
			<h2 class="title title_lg font-bold "><?php echo $feat_updates_params['title'] ?></h2>
		</header>
		<div class="postList row">
			<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<?php include( locate_template( 'template_parts/_update.php', false, false ) ); ?>
			<?php endwhile; ?>
		</div>

		<div class="section_cto text-<?php echo $feat_updates_params['button_alignment'] ?>">
			<a href="<?php echo get_site_url(); ?>/news-and-updates" class="btn btn-primary">
				Load More
			</a>
		</div>
	<?php else : ?>
		<p class="title font-bold">There are no <?php echo $feat_updates_params['title'] ?> available at the moment.</p>
	<?php endif; ?>
	</div>
</section>
