<article class="col-sm-6 col-md-4 cardItem">
	<a href="<?php echo rwmb_meta( 'download_link' ) ?>" class="cardWrapper fileCardWrapper" target="_blank">
		<div class="cardInfoPrimary">
			<div class="cardMetaRow">
				<div class="metaItem">
					<span><?php  echo get_first_term('document-category'); ?></span>
				</div>
			</div>
			<h4 class="title title_sm color__inverse font-bold"><?php the_title() ?></h4>
		</div>
		<div class="cardinfoFooter">
			<div class="color__secondary">
				<span><?php  echo get_first_term('file-type').'. '.rwmb_meta( 'size' ); ?></span>
			</div>
			<div>
				<i class="cust_icon icon_download"></i>
			</div>
		</div>
	</a>
</article>
