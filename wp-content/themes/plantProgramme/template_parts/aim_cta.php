<?php 
	rwmb_meta( 'cta_bg', array( 'object_type' => 'setting' ), 'my_options'); 
	$image = 0;
	$images = rwmb_meta( 'cta_bg', array( 'object_type' => 'setting', 'limit' => 1 ), 'my_options'); 
	$image = reset( $images );
?>
<section class="hero_area footer_hero" style="background-image: url('<?php echo $image['full_url']; ?>') !important;">
	<div class="container">
		<div class="row heroContentWrapper">
			<div class="col-md-6">
				<div class="heroContentBoxWrapper bg__primary color__white">
					<h2 class="title title_lg font-bold margin_bottom_md"><?php echo rwmb_meta( 'cta_title', array( 'object_type' => 'setting' ), 'my_options'); ?></h2>
					<p><?php echo rwmb_meta( 'cta_content', array( 'object_type' => 'setting' ), 'my_options'); ?></p>
					<div class="section_cto">
						<a href="<?php echo rwmb_meta( 'cta_link', array( 'object_type' => 'setting' ), 'my_options'); ?>" class="btn btn-default-outline">
							<?php echo rwmb_meta( 'cta_text', array( 'object_type' => 'setting' ), 'my_options'); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
