<?php if($terms_array){ ?>
<nav class="filterBar bg__default">
	<div class="container">
		<form class="filterOptionRow" method="get" action="<?php the_permalink() ?>">
			<?php foreach ( $terms_array as $term_item ) { ?>
				<?php $terms = get_terms( $term_item['slug'], array('hide_empty' => false)); ?>
				<article class="filterOption">
					<select class="filterControl" name="<?php echo $term_item['slug'] ?>">
						<option value=""><?php echo $term_item['title'] ?></option>
						<?php foreach ($terms as $term) { ?>
							<option value="<?php echo $term->slug ?>" <?php echo ( $_GET[$term_item['slug']] == $term->slug ) ? 'selected' : ''; ?>><?php echo $term->name ?></option>
						<?php } ?>
					</select>
				</article>
			<?php } ?>
			<button class="btn btn-primary ml-4">Filter</button>
		</form>
	</div>
</nav>
<?php } ?>