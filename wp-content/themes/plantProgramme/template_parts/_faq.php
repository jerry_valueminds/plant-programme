<article class="panel panel-FAQs">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#questionPanel" href="#question_1">What is PLANT?</a>
		</h4>
	</div>
	<div id="question_1" class="panel-collapse collapse ">
		<div class="panel-body">
			<p>PLANT will support Incubation hubs with advisory services to improve their programmes and service offering to AgTech entrepreneurs helping them to identify and exploit opportunities in the agriculture sector.
			</p>
		</div>
	</div>
</article>
