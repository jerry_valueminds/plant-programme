<?php 
	$endOfDay   = strtotime("yesterday");
	$args = array(
		'post_type' => 'event',
		'post_status' => 'publish',
		'posts_per_page' => 4,
		'meta_query' => array(
			array(
				'key'     => 'expiry_date',
				'value'   => $endOfDay,
				'compare' => '>',
			),
		)
	);
	$custom_posts = new WP_Query( $args );
?>

<?php if ( $custom_posts->have_posts() ) : ?> 
<section class="section_block bg__white">
	<div class="container ">
		<header class="section_header text-center">
			<h2 class="title title_lg font-bold color__inverse">Upcoming Events</h2>
		</header>

		<div class="eventsList panel-group" id="eventsList">
			<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<?php include( locate_template( 'template_parts/_event.php', false, false ) ); ?>
			<?php endwhile; ?>
		</div>
		<div class="section_cto text-center">
			<a href="<?php echo get_site_url(); ?>/events" class="btn btn-primary">
				View All
			</a>
		</div>
	</div>
</section>
<?php endif; ?>