<article class="eventItem">
	<div class="eventTileWrapper">
		<a href="#event_<?php echo get_the_ID() ?>" data-toggle="collapse" data-parent="eventsList" class="accordion-toggle collapsed">
			<div class="eventListDetails">
				<div class="eventCardTime color__primary font-bold">
					<time>
						<?php 
							$value = rwmb_meta( 'start_date' );
							echo date( 'M j', $value );
						
							$value = rwmb_meta( 'end_date' );
							echo ' - '.date( 'M j', $value );
			 			 
			 				echo date(" . h:ia ", strtotime( rwmb_meta( 'start_time' ) ));
			 				echo date(" - h:ia . ", strtotime( rwmb_meta( 'end_time' ) ));
			 
			 				$term_list = wp_get_post_terms( get_the_ID(), 'location', array( 'fields' => 'names' ) );
							list_terms( $term_list );
						?>
					</time>
				</div>
				<h1 class="title title_sm color-black font-bold"><?php the_title() ?></h1>
			</div>
		</a>
	</div>
	<div class="panel-collapse collapse" id="event_<?php echo get_the_ID() ?>">
		<div class="panel-body">
			<p><?php echo rwmb_meta( 'summary' ) ?></p>
			<div class="section_cto">
				<a href="<?php echo rwmb_meta( 'application_link' ) ?>" class="btn btn-sm btn-primary">Learn More</a>
			</div>
		</div>
	</div>
</article>