<?php /*Template Name: Downloads*/ ?>
<?php get_header() ?>
<?php while ( have_posts() ) : the_post(); ?>
<main class="mainContentArea bg__off_white ">
	<section class="section_block bg__white">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 heroContentBox">
					<h2 class="title title_md font-bold color__inverse margin_bottom_sm">Downloads</h2>
					<p class="font-md color__inverse"><?php echo rwmb_meta( 'page_excerpt' ) ?></p>
				</div>
			</div>
		</div>
	</section>
	<?php 
		$terms_array = array(
			array(
				'title'	=>	'FIle type',
				'slug'	=>	'file-type'
			),
			array(
				'title'	=>	'Category',
				'slug'	=>	'document-category'
			),
			array(
				'title'	=>	'Intervention',
				'slug'	=>	'intervention'
			),
		) 
	?>
	<?php include( locate_template( 'template_parts/post_filter.php', false, false ) ); ?>
	<section class="section_block ">
		<div class="container">
			<?php 
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' => 'download',
					'post_status' => 'publish',
					'posts_per_page' => 9,
					'paged' => $paged,
					'tax_query' => get_tax_array()
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
			<div class="row">
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<?php include( locate_template( 'template_parts/_download.php', false, false ) ); ?>
				<?php endwhile; ?>
			</div>
			<div class="section_cto pager text-center">
				<nav class="pager_nav">
					<?php numbered_pagination( $custom_posts ); ?>
				</nav>
			</div>
			<?php else : ?>
				<p class="title font-bold">There are no updates available at the moment.</p>
			<?php endif; ?>
		</div>
	</section>
</main>
<?php endwhile; ?>
<?php get_footer() ?>
