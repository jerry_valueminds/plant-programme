<?php /*Template Name: Homepage*/ ?>
<?php get_header() ?>
<?php 
	while ( have_posts() ) : the_post();
		$section_one_data = rwmb_meta('homepage_section_one');
		$section_two_data = rwmb_meta('homepage_section_two');
		$section_three_data = rwmb_meta('homepage_section_three');
	endwhile; 
?>
<main class="mainContentArea bg__off_white">
	<?php 
			$args = array(
				'post_type' => 'slide',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key'     => 'hidden',
						'value'   => '1',
						'compare' => '!=',
					),
				)
			);
			$custom_posts = new WP_Query( $args );
  ?>
  <?php if ( $custom_posts->have_posts() ) : ?> 
	<section id="carouselExampleIndicators" class="carousel homeSlider slide" data-ride="carousel">
		<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
		<div class="carousel-item heroSlideItem <?php echo !$custom_posts->current_post ? 'active' : '' ?>">
			<img src="<?php echo get_metabox_image_url('picture') ?>" class="carouselBanner" alt="<?php echo get_metabox_image_alt('picture') ?>">
			<div class="carouselContentInner">
				<div class="container">
					<div class="row heroContentWrapper">
						<div class="col-md-6 heroContentBox box_floated_down">
							<div class="heroContentBoxWrapper color__white">
								<ol class="carousel-indicators">
									<?php for( $x = 0; $x < $custom_posts->found_posts; $x++ ) { ?>
										<li 
											data-target="#carouselExampleIndicators" 
											data-slide-to="<?php echo $x ?>" 
											class="<?php echo ($custom_posts->current_post == $x ) ? 'active' : '' ?>">
										</li>
									<?php } ?>
								</ol>
								<h2 class="title title_lg font-bold font-weight-bold margin_bottom_md"><?php the_title() ?></h2>
								<p class="font-weight-bold"><?php echo rwmb_meta( 'summary' ) ?></p>
								<div class="section_cto">
									<a href="<?php echo rwmb_meta( 'application_link' ) ?>" class="btn btn-primary" target="_blank">Apply Now</a>
									<a href="<?php echo rwmb_meta( 'learn_more_link' ) ?>" class="btn btn-default-outline">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</section>
	<?php endif; ?>
	
	<section class="section_block bg__white">
		<div class="bannerBlobItem homeBlobTopLeft"></div>
		<div class="container ">
			<div class="row text-center color_text_alt">
				<div class="offset-md-3 col-md-6">
					<header class="section_header">
						<h2 class="title title_lg font-bold color__primary margin_bottom_md"><?php echo $section_one_data['homepage_section_one_title']; ?></h2>
						<p><?php echo $section_one_data['homepage_section_one_content']; ?></p>
					</header>
					<?php include( locate_template( 'template_parts/sponsors.php', false, false ) ); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="section_block ">
		<div class="bannerBlobItem homeBlobTopRight"></div>
		<div class="container">
			<div class="row text-center color_text_alt">
				<div class="offset-md-3 col-md-6">
					<header class="section_header">
						<span class="headerSupport caps_upper"><?php echo $section_two_data['subtitle']; ?></span>
						<h2 class="title title_lg font-bold color__primary margin_bottom_md margin_top_sm"><?php echo $section_two_data['title']; ?></h2>
						<p><?php echo $section_two_data['content']; ?></p>
					</header>
				</div>
			</div>
			<?php if( count( $section_two_data['list'] ) ){ ?>
				<div class="row">
				<?php foreach( $section_two_data['list'] as $list_item ){ ?>
					<article class="col-md-4 featureListItem color_text_alt">
						<header class="featHeader featIconHr color__grey_dark">
							<?php if( get_metabox_group_image_url( $list_item, 'icon' ) ){ ?>
								<div class="feat_Icon">
									<img src="<?php echo get_metabox_group_image_url( $list_item, 'icon' ); ?>" alt="<?php echo get_metabox_group_image_alt( $list_item, 'icon' ); ?>" >
								</div>
							<?php } ?>
							<h4 class="featTitle color__black"><?php echo $list_item['title']; ?></h4>
						</header>
						<p><?php echo $list_item['content']; ?></p>
					</article>
				<?php } ?>
				</div>
			<?php } ?>
			<div class="section_cto text-center">
				<a href="<?php echo $section_two_data['learn_more']; ?>" class="btn-link">
					<span class="btnLabel">Learn More</span>
					<span class="btnIcon">
						<svg class="cust_icon" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<rect width="16" height="16" fill="white" fill-opacity="0.01" />
							<path d="M10.5858 9.00001H2V7.00001H10.5858L7.29289 3.70712L8.70711 2.29291L14.4142 8.00001L8.70711 13.7071L7.29289 12.2929L10.5858 9.00001Z" />
						</svg>
					</span>
				</a>
			</div>
		</div>
	</section>
	
	<?php 
		$args = array(
			'post_type' => 'intervention',
			'post_status' => 'publish',
			'posts_per_page' => 3,
		);
		$custom_posts = new WP_Query( $args );
	?>
  <?php if ( $custom_posts->have_posts() ) : ?> 
	<section class="section_block bg__black">
		<div class="container ">
			<div class="row text-center color__white">
				<div class="offset-md-3 col-md-6">
					<header class="section_header">
						<h2 class="title title_md font-bold margin_bottom_md"><?php echo $section_three_data['homepage_section_three_title']; ?></h2>
						<p><?php echo $section_three_data['homepage_section_three_content']; ?></p>
					</header>
				</div>
			</div>
			<div class="row tileCardList">
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
					<article class="tileCardItem heightFixed col-md-4">
						<div class="tileCardWrapper" style="background-color: <?php echo rwmb_meta( 'color' ) ?>;">
							<div class="tileCardinfo color__white">
								<h4 class="title title_sm font-bold margin_bottom_md"><?php the_title() ?></h4>
								<p><?php echo rwmb_meta( 'highlight_content' ) ?></p>
							</div>
							<div class="tileCardCTO margin_top_lg">
								<a href="<?php the_permalink() ?>" class="btn-link color__white">
									<span class="btnLabel">Learn More</span>
									<span class="btnIcon">
										<svg class="cust_icon" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
											<rect width="16" height="16" fill="white" fill-opacity="0.01" />
											<path d="M10.5858 9.00001H2V7.00001H10.5858L7.29289 3.70712L8.70711 2.29291L14.4142 8.00001L8.70711 13.7071L7.29289 12.2929L10.5858 9.00001Z" />
										</svg>
									</span>
								</a>
							</div>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
			<div class="section_cto text-center">
				<a href="<?php echo get_site_url(); ?>/apply" class="btn btn-primary">
					View Open Cohorts
				</a>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<?php include( locate_template( 'template_parts/upcoming_events.php', false, false ) ); ?>
	<?php include( locate_template( 'template_parts/updates.php', false, false ) ); ?>
	<?php include( locate_template( 'template_parts/aim_cta.php', false, false ) ); ?>
</main>

<?php get_footer() ?>
