<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php $author = rwmb_meta( 'author' ); ?>
<style>
	ul.heateor_sss_follow_ul, ul.heateor_sss_sharing_ul {
		display: flex !important;
		justify-content: center !important;
	}
	
	ul.heateor_sss_follow_ul li:not(:first-child), ul.heateor_sss_sharing_ul li:not(:first-child){
		margin-left: 20px !important;
	}
</style>
<main class="mainContentArea bg__off_white ">
	<section class="post_mast_head bg__white">
		<div class="container">
			<div class="row">
				<div class="heroContentBox text-center offset-md-2 col-md-8">
					<div class="margin_bottom_sm">
						<span class="postTypeTag"><?php echo get_first_term( 'blog-category' ) ?></span>
					</div>
					<h2 class="title title_md font-bold"><?php the_title() ?></h2>
					<div class="media_item margin_top_sm">
						<div class="media_figure">
							<div class="user_info_avi avi_lg">
								<?php 
									$images = rwmb_meta( 'picture', array( 'size' => 'thumbnail', 'limit' => 1 ) );
									$image = reset( $images );
								?>
								<img src="<?php echo $image['full_url'] ?>" alt="Author image">
							</div>
						</div>
						<div class="media_info">
							<span class="color__grey_dark">by <?php echo get_the_title($author); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="contentRow bg__white">
		<div class="featuredImage">
			<div class="container content_narrow">
				<figure>
					<img src="<?php echo get_metabox_image_url('picture') ?>" alt="<?php echo get_metabox_image_alt('picture') ?>">
				</figure>
				<div class="margin_top_lg text-center">
					<?php echo do_shortcode('[Sassy_Social_Share]') ?>
				</div>
			</div>
		</div>
		<div class="postArticleContentArea">
			<div class="container blogLayoutWrapper">
				<div class="postArticle"><?php echo rwmb_meta( 'content' ) ?></div>
				<div class="postInfoWidget postFooterNote">
					<span class="headermarker"></span>
					<h4 class="title title_sm color__grey_dark">Published on <?php echo get_the_date( 'F j, Y' ) ?></h4>
					<p>
						<strong class="color__primary"><?php echo get_the_title($author); ?></strong> 
						<?php echo rwmb_meta( 'summary', array(), $author ); ?>
					</p>
				</div>
			</div>
		</div>
		<section class="contentRow padding_bottom_none">
			<div class="container">
				<?php 
					$args = array(
						'post_type' => 'update',
						'post_status' => 'publish',
						'posts_per_page' => 3,
						'post__not_in' => array( Get_the_ID() ),
					);

					$custom_posts = new WP_Query( $args );
				?>
				<?php if ( $custom_posts->have_posts() ) : ?> 
					<div class="postList row">
						<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
							<?php include( locate_template( 'template_parts/_update.php', false, false ) ); ?>
						<?php endwhile; ?>
					</div>
					<div class="section_cto text-center">
						<a href="<?php echo get_site_url(); ?>/news-and-updates" class="btn btn-primary">View All</a>
					</div>
				<?php else : ?>
					<p class="title font-bold">There are no updates available at the moment.</p>
				<?php endif; ?>
			</div>
		</section>
	</section>
</main>

<?php endwhile; ?>

<?php get_footer() ?>
