<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>
<style>
	ul.heateor_sss_follow_ul li:not(:first-child), ul.heateor_sss_sharing_ul li:not(:first-child){
		margin-left: 20px !important;
	}
</style>
<main class="mainContentArea bg__off_white ">
	<section class="post_mast_head bg__white">
		<div class="container">
			<div class="heroContentBox">
				<div class="margin_bottom_sm">
					<span class="postTypeTag">JOB OPENING</span>
				</div>
				<h2 class="title title_md font-bold color__secondary"><?php the_title() ?></h2>
			</div>
		</div>
	</section>
	<section class="postContentBlock ">
		<div class="container ">
			<div class="contentRow row">
				<section class="postContentArea col-md-8">
					<div class="postArticle"><?php echo rwmb_meta( 'description' ) ?></div>
					<div class="section_cto">
						<a href="<?php echo rwmb_meta( 'application_link' ) ?>" class="btn btn-primary">Apply</a>
					</div>
				</section>
				<nav class="postSidebar col-md-4">
					<div class="postControlList">
						<?php $term_list = wp_get_post_terms( get_the_ID(), 'job-type', array( 'fields' => 'names' ) ); ?>
						<?php if ( $term_list[0] ){ ?>
							<?php foreach ( $term_list as $term_item ){ ?>
								<div class="postControlItem">
									<div class="postControlBtn text-uppercase"><?php echo $term_item ?></div>
								</div>
							<?php } ?>
						<?php } ?>
						
						<?php $term_list = wp_get_post_terms( get_the_ID(), 'job-area', array( 'fields' => 'names' ) ); ?>
						<?php if ( $term_list[0] ){ ?>
							<?php foreach ( $term_list as $term_item ){ ?>
								<div class="postControlItem">
									<div class="postControlBtn bg__default text-uppercase"><?php echo $term_item ?></div>
								</div>
							<?php } ?>
						<?php } ?>
						<div class="postControlItem">
							<div class="postControlBtn bg__secondary color__primary_light">
								<span>
									Expires: 
									<time>
										<?php 
											$value = rwmb_meta( 'expiry_date' );
											echo date( 'M j,Y', $value );
										?>
									</time>
								</span>
							</div>
						</div>
						<div class="postControlItem">
							<?php 
								$files = rwmb_meta( 'document', array( 'limit' => 1 ) );
								$file = reset( $files );
							?>

							<a href="<?php echo $file['url']; ?>" class="postControlBtn bg__white" target="_blank">
								<span>Download Documents</span>
								<span>
									<i class="cust_icon icon_download_green"></i>
								</span>
							</a>
						</div>
						<div class="margin_top_lg">
							<?php echo do_shortcode('[Sassy_Social_Share]') ?>
						</div>
					</div>
				</nav>
			</div>
			<div class="contentRow">
				<?php 
					$endOfDay   = strtotime("yesterday");
					$args = array(
						'post_type' => 'job',
						'post_status' => 'publish',
						'posts_per_page' => 3,
						'post__not_in' => array( Get_the_ID() ),
						'meta_query' => array(
							array(
								'key'     => 'expiry_date',
								'value'   => $endOfDay,
								'compare' => '>',
							),
						)
					);

					$custom_posts = new WP_Query( $args );
				?>
				<?php if ( $custom_posts->have_posts() ) : ?> 
					<header class="margin_bottom_md">
						<h3 class="title title_sm color__secondary  font-bold">Other Jobs</h3>
					</header>
					<div class="jobList row">
						<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
							<?php include( locate_template( 'template_parts/_job.php', false, false ) ); ?>
						<?php endwhile; ?>
					</div>
					<div class="section_cto">
						<a href="<?php echo get_site_url(); ?>/jobs" class="btn btn-primary">view All</a>
					</div>
				<?php else : ?>
					<p class="title font-bold">There are no jobs available at the moment.</p>
				<?php endif; ?>
			
			</div>
		</div>
	</section>
</main>

<?php endwhile; ?>

<?php get_footer() ?>
