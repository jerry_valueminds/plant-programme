<?php /*Template Name: Contact Us*/ ?>
<?php get_header() ?>

<style>
	.wpcf7 p {
		margin-bottom: 0 !important;
	}

	.wpcf7 .ajax-loader {}

	.wpcf7-validation-errors {
		color: red;
		padding: 0 !important;
		margin: 0 !important;
		border: 0 !important;
	}

	.wpcf7-mail-sent-ok {
		color: green;
		padding: 0 !important;
		margin: 0 !important;
		border: 0 !important;
	}
</style>
<?php while ( have_posts() ) : the_post(); ?>
<main class="mainContentArea bg__off_white">
	<section class="hero_area section_block contact_hero">
		<div class="container">
			<div class="heroContentBoxWrapper bg__white">
				<fieldset>
					<div class="row form_row_group">
						<div class="form_group_item col-md-4">
							<h4 class="title title_md font-bold">Visit us</h4>
						</div>
						<div class="form_group_item col-md-8">
							<div class="contactInfoBlock color__grey_dark">
								<h4 class="contactHeader ">Technoserve Nigeria</h4>
								<address class="contactAddress">
									<p><?php echo rwmb_meta( 'address', array( 'object_type' => 'setting' ), 'my_options'); ?></p>
								</address>
								<div class="contactLinks">
									<?php 
										$contact_email = rwmb_meta( 'email', array( 'object_type' => 'setting' ), 'my_options');
										$contact_phone = rwmb_meta( 'phone', array( 'object_type' => 'setting' ), 'my_options');
									?>
									<a href="tel:<?php echo $contact_phone ?>"><?php echo $contact_phone ?></a> 
									<span class="inline_seperator">.</span> 
									<a href="mailto:<?php echo $contact_email ?>"><?php echo $contact_email ?></a>
								</div>
							</div>

							<div class="social_links contactInfoBlock">
								<a href="<?php echo rwmb_meta( 'site_facebook', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank">
									<svg xmlns="http://www.w3.org/2000/svg" class="cust_icon" viewBox="0 0 64 64">
										<path class="st0" d="M45.1,31.8l-8.4,0.1L37,62.6l-12.7,0.1L24,32l-6,0.1l-0.1-10.8l6-0.1l-0.1-7c-0.1-5,2.2-12.8,12.7-13l9.4-0.1L46,11.7l-6.8,0.1c-1.1,0-2.7,0.6-2.7,3l0.1,6.4l9.5-0.1L45.1,31.8z M45.1,31.8" />
									</svg>
								</a>
								<a href="<?php echo rwmb_meta( 'site_twitter', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank" title="Follow in twitter">
									<svg xmlns="http://www.w3.org/2000/svg" class="cust_icon" viewBox="0 0 64 64">
										<path d="M64,12.2c-2.4,1-4.9,1.8-7.5,2.1c2.7-1.6,4.8-4.2,5.8-7.3c-2.5,1.5-5.3,2.6-8.3,3.2C51.5,7.6,48.1,6,44.3,6c-7.3,0-13.1,5.9-13.1,13.1c0,1,0.1,2,0.3,3C20.6,21.6,10.9,16.3,4.5,8.4c-1.1,1.9-1.8,4.2-1.8,6.6c0,4.6,2.3,8.6,5.8,10.9c-2.2-0.1-4.2-0.7-5.9-1.6c0,0.1,0,0.1,0,0.2c0,6.4,4.5,11.7,10.5,12.9c-1.1,0.3-2.3,0.5-3.5,0.5c-0.8,0-1.7-0.1-2.5-0.2c1.7,5.2,6.5,9,12.3,9.1C15,50.2,9.3,52.3,3.1,52.3c-1.1,0-2.1-0.1-3.1-0.2C5.8,55.8,12.7,58,20.1,58c24.1,0,37.4-20,37.4-37.4c0-0.6,0-1.1,0-1.7C60,17.1,62.2,14.8,64,12.2L64,12.2z M64,12.2" />
									</svg>
								</a>
								<a href="<?php echo rwmb_meta( 'site_instagram', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank" title="Follow on Instagram">
									<svg class="cust_icon" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
										<path d="M16.7145 -0.000976562H7.71448C3.57298 -0.000976562 0.214478 3.35752 0.214478 7.49902V16.499C0.214478 20.6405 3.57298 23.999 7.71448 23.999H16.7145C20.856 23.999 24.2145 20.6405 24.2145 16.499V7.49902C24.2145 3.35752 20.856 -0.000976562 16.7145 -0.000976562ZM21.9645 16.499C21.9645 19.394 19.6095 21.749 16.7145 21.749H7.71448C4.81948 21.749 2.46448 19.394 2.46448 16.499V7.49902C2.46448 4.60402 4.81948 2.24902 7.71448 2.24902H16.7145C19.6095 2.24902 21.9645 4.60402 21.9645 7.49902V16.499Z" />
										<path d="M12.2145 5.99902C8.90098 5.99902 6.21448 8.68552 6.21448 11.999C6.21448 15.3125 8.90098 17.999 12.2145 17.999C15.528 17.999 18.2145 15.3125 18.2145 11.999C18.2145 8.68552 15.528 5.99902 12.2145 5.99902ZM12.2145 15.749C10.1475 15.749 8.46448 14.066 8.46448 11.999C8.46448 9.93052 10.1475 8.24902 12.2145 8.24902C14.2815 8.24902 15.9645 9.93052 15.9645 11.999C15.9645 14.066 14.2815 15.749 12.2145 15.749Z" />
										<path d="M18.6645 6.3485C19.106 6.3485 19.464 5.99056 19.464 5.54901C19.464 5.10746 19.106 4.74951 18.6645 4.74951C18.2229 4.74951 17.865 5.10746 17.865 5.54901C17.865 5.99056 18.2229 6.3485 18.6645 6.3485Z" />
									</svg>
								</a>
							</div>
							<div class="contactInfoBlock">
								<a href="<?php echo rwmb_meta( 'map_link', array( 'object_type' => 'setting' ), 'my_options'); ?>" class="btn btn-primary">Get Direction on Google Maps</a>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row form_row_group">
						<div class="form_group_item col-md-4">
							<h4 class="title title_md font-bold">Ask a <br> Question</h4>
						</div>
						<div class="form_group_item col-md-8">
							<?php echo do_shortcode('[contact-form-7 id="5" title="Contact Form"]'); ?>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</section>
</main>
<?php endwhile; ?>
<?php get_footer() ?>
