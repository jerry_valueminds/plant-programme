<footer role="contentinfo" class="footerArea">
	<div class="container">
		<section class="footerTop footerRow">
			<div class="row">
				<div class="col-6 col-sm-4 col-md-3 footer_block">
					<div class="ft_block_header">
						<h3 class="item_title">Interventions</h3>
					</div>
					<div class="ft_block_segment">
						<ul class="ft_nav_block">
						<?php 
							$args = array(
								'post_type' => 'intervention',
								'post_status' => 'publish',
								'posts_per_page' => 4,
							);
							$custom_posts = new WP_Query( $args );
						?>
						<?php if ( $custom_posts->have_posts() ) : ?> 
							<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
								<li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
							<?php endwhile; ?>
						<?php endif; ?>
						</ul>
					</div>
				</div>
				<div class="col-6 col-sm-4 col-md-3 footer_block">
					<div class="ft_block_header">
						<h3 class="item_title">Company</h3>
					</div>
					<div class="ft_block_segment">
						<ul class="ft_nav_block">
							<li><a href="<?php echo get_site_url(); ?>/about">About</a></li>
							<li><a href="<?php echo get_site_url(); ?>/ain">About AIN</a></li>
							<li><a href="<?php echo get_site_url(); ?>/jobs">Careers</a></li>
							<li><a href="<?php echo get_site_url(); ?>/contact-us">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-6 col-sm-4 col-md-3 footer_block">
					<div class="ft_block_header">
						<h3 class="item_title">More</h3>
					</div>
					<div class="ft_block_segment">
						<ul class="ft_nav_block">
							<li><a href="<?php echo get_site_url(); ?>/events">Events</a></li>
							<li><a href="<?php echo get_site_url(); ?>/news-and-updates">News &amp; Updates</a></li>
							<li><a href="<?php echo get_site_url(); ?>/downloads">Downloads</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tenders">Tenders</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-3 footer_block">
					<div class="ft_block_header">
						<h3 class="item_title">Stay Connected</h3>
					</div>
					<div class="ft_block_segment">
						<p>Join our mailing list to get the latest updates, we promise not to spam you.</p>
						<form class="subscribeForm" action="#">
							<input type="email" name="email" required placeholder="Your Email Addresss" class="form-control subscribe-email insetInput">
							<div class="margin_top_md">
								<button type="submit" class="btn btn-block btn-primary subscribe-button">Subscribe to Newsletter</button>
							</div>
							<p class="subscribe-status pt-4" style="line-height:1.2em"></p>
						</form>
					</div>
				</div>
			</div>
		</section>
		<section class="footerBottom footerRow">
			<div class="licenseBlock">
				<p class="copyright_notice">&copy; Copyright <?php echo date('Y'); ?> - Powered by <span class="iconBrandParent">TechnoServe</span></p>
			</div>
			<div class="socialsBlock">
				<div class="social_links">
					<a href="<?php echo rwmb_meta( 'site_facebook', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" class="cust_icon" viewBox="0 0 64 64">
							<path class="st0" d="M45.1,31.8l-8.4,0.1L37,62.6l-12.7,0.1L24,32l-6,0.1l-0.1-10.8l6-0.1l-0.1-7c-0.1-5,2.2-12.8,12.7-13l9.4-0.1L46,11.7l-6.8,0.1c-1.1,0-2.7,0.6-2.7,3l0.1,6.4l9.5-0.1L45.1,31.8z M45.1,31.8" />
						</svg>
					</a>
					<a href="<?php echo rwmb_meta( 'site_twitter', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank" title="Follow in twitter">
						<svg xmlns="http://www.w3.org/2000/svg" class="cust_icon" viewBox="0 0 64 64">
							<path d="M64,12.2c-2.4,1-4.9,1.8-7.5,2.1c2.7-1.6,4.8-4.2,5.8-7.3c-2.5,1.5-5.3,2.6-8.3,3.2C51.5,7.6,48.1,6,44.3,6c-7.3,0-13.1,5.9-13.1,13.1c0,1,0.1,2,0.3,3C20.6,21.6,10.9,16.3,4.5,8.4c-1.1,1.9-1.8,4.2-1.8,6.6c0,4.6,2.3,8.6,5.8,10.9c-2.2-0.1-4.2-0.7-5.9-1.6c0,0.1,0,0.1,0,0.2c0,6.4,4.5,11.7,10.5,12.9c-1.1,0.3-2.3,0.5-3.5,0.5c-0.8,0-1.7-0.1-2.5-0.2c1.7,5.2,6.5,9,12.3,9.1C15,50.2,9.3,52.3,3.1,52.3c-1.1,0-2.1-0.1-3.1-0.2C5.8,55.8,12.7,58,20.1,58c24.1,0,37.4-20,37.4-37.4c0-0.6,0-1.1,0-1.7C60,17.1,62.2,14.8,64,12.2L64,12.2z M64,12.2" />
						</svg>
					</a>
					<a href="<?php echo rwmb_meta( 'site_instagram', array( 'object_type' => 'setting' ), 'my_options'); ?>" target="_blank" title="Follow on Instagram">
						<svg class="cust_icon" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
							<path d="M16.7145 -0.000976562H7.71448C3.57298 -0.000976562 0.214478 3.35752 0.214478 7.49902V16.499C0.214478 20.6405 3.57298 23.999 7.71448 23.999H16.7145C20.856 23.999 24.2145 20.6405 24.2145 16.499V7.49902C24.2145 3.35752 20.856 -0.000976562 16.7145 -0.000976562ZM21.9645 16.499C21.9645 19.394 19.6095 21.749 16.7145 21.749H7.71448C4.81948 21.749 2.46448 19.394 2.46448 16.499V7.49902C2.46448 4.60402 4.81948 2.24902 7.71448 2.24902H16.7145C19.6095 2.24902 21.9645 4.60402 21.9645 7.49902V16.499Z" />
							<path d="M12.2145 5.99902C8.90098 5.99902 6.21448 8.68552 6.21448 11.999C6.21448 15.3125 8.90098 17.999 12.2145 17.999C15.528 17.999 18.2145 15.3125 18.2145 11.999C18.2145 8.68552 15.528 5.99902 12.2145 5.99902ZM12.2145 15.749C10.1475 15.749 8.46448 14.066 8.46448 11.999C8.46448 9.93052 10.1475 8.24902 12.2145 8.24902C14.2815 8.24902 15.9645 9.93052 15.9645 11.999C15.9645 14.066 14.2815 15.749 12.2145 15.749Z" />
							<path d="M18.6645 6.3485C19.106 6.3485 19.464 5.99056 19.464 5.54901C19.464 5.10746 19.106 4.74951 18.6645 4.74951C18.2229 4.74951 17.865 5.10746 17.865 5.54901C17.865 5.99056 18.2229 6.3485 18.6645 6.3485Z" />
						</svg>
					</a>
				</div>
			</div>
		</section>
	</div>
</footer>

<div class="modal fade" id="subscribe_modal">
	<div class="modal-dialog postDialog">
		<div class="modal-content">
			<header class="modal-header">
				<div class="dialogDismiss">
					<button type="button" class="close_dialog" data-dismiss="modal"></button>
				</div>
			</header>
			<div class="modal-body">
				<form class="subscribeForm" action="#">
					<h2 class="title title_md font-bold color__primary">Stay Connected</h2>
					<div class="postArticle margin_bottom_sm">Join our mailing list to get the latest updates, we promise not to spam you.</div>
					<input type="email" name="email" required placeholder="Your Email Addresss" class="form-control subscribe-email insetInput mb-3">
					<button type="submit" class="btn btn-block btn-primary subscribe-button">Subscribe to Newsletter</button>
					<p class="subscribe-status pt-2" style="line-height:1.2em"></p>
				</form>
			</div>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

<script>
	$(document).ready(function(){
		$('.heroContentSlider').slick({
			dots: true,
			infinite: true,
			swipe: true,
			arrows: false,
			nav: false,
			speed: 1000,
			slidesToShow: 1
		});
	});
</script>

<script>
	$(document).ready(function(){
		if( !localStorage.getItem("notFirstVisit") ){
			$('#subscribe_modal').modal('show');
			localStorage.setItem("notFirstVisit", "true");
		}
	});
</script>

<script>
	$(document).ready(function(){
		
		$('.subscribeForm').submit(function(e){
			e.preventDefault();
			var subscribeEmail = $(this).find('.subscribe-email');
			var subscribeButton = $(this).find('.subscribe-button');
			var subscribeStatus = $(this).find('.subscribe-status');
			subscribeStatus.text("Pleas wait...");
			subscribeStatus.css("color", "green");
			
			const email = subscribeEmail.val();
			subscribeButton.prop('disabled', true);

			$.post("https://maylbox.com/subscribe", {email: email, list: 'M892stTa23UyaOcqXc4wsH4g'}, function(data){
				if(data){
					if(data=="Some fields are missing."){
						subscribeStatus.text("Please fill in your email.");
						subscribeStatus.css("color", "red");
					}else if(data=="Invalid email address."){
						subscribeStatus.text("Your email address is invalid.");
						subscribeStatus.css("color", "red");
					}else if(data=="Invalid list ID."){
						subscribeStatus.text("Your list ID is invalid.");
						subscribeStatus.css("color", "red");
					}else if(data=="Already subscribed."){
						subscribeStatus.text("You're already subscribed!");
						subscribeStatus.css("color", "red");
						$('#subscribe-email').val('');
					}else{
						subscribeStatus.text("Thank you for subscribing to our mailing list.");
						subscribeStatus.css("color", "green");
						subscribeEmail.val('');
					}
				}else{
					alert("Sorry, unable to subscribe. Please try again later!");
				}
				subscribeButton.prop('disabled', false);
			});
		})	
		
	});
</script>

</body>

</html>
