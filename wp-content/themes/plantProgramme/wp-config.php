<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'b052893ef90dd9cf2d79e7459e00ec56c280ca83c9dd0024' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':1**gKK#`Eg{Zf2&OR-7`}3sqW&.Z)hxPKx5`wE/Yg}Ka=-H6cdX$.oNLb4H~;f!' );
define( 'SECURE_AUTH_KEY',  '1I(_I^QDZ@*QCXfKP!zZN:stPW~EsL!?m~5.Arq8r^AEZkBEY(23vhhll(<Q~xdB' );
define( 'LOGGED_IN_KEY',    '< (dG~SBrAF~UQ?rfYA`P}[Y&y?VL~EQs2b650TGtGu_gqm)D5&FjX>9aj=O6tTi' );
define( 'NONCE_KEY',        '+?L>3i!s16~OOlU+WR .vF6eX5,0&zkw}#!+/Rz6aG]MTc8GJy1TDD=:CWCr(g+t' );
define( 'AUTH_SALT',        '5_FN;u,pSBe&L+Lr(pxV.sZf&rs,d`rO<_.,.(eO4R2_Nl5WwYD_iDRgRuvoD}[_' );
define( 'SECURE_AUTH_SALT', 'Ekuf?le>@(g{j+#8sK.0ciG&t9Q;MlTyZ1?NhF,D<iTdQ0,#Wg|r})I%-pXzsuQe' );
define( 'LOGGED_IN_SALT',   ':?H+</HeiI5oE0zmF(A}[@I&0[d9OiI>v=5Z0(.7O( 2Qc(6ry_!q$:?2HoamGC;' );
define( 'NONCE_SALT',       '`bnUndhakBD:ZmW{RI`L/Sqp.1[?p}hR-6Jz[aJ.<.=tpebb5.JGXlt0cM)@+o)^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Allow file uploads */
define('ALLOW_UNFILTERED_UPLOADS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
