<?php
add_filter( 'mb_settings_pages', 'prefix_settings_pages' );
function prefix_settings_pages( $settings_pages ) {
    $settings_pages[] = array(
        'id'            => 'my-options',
        'menu_title'    => 'Theme Options',
        'option_name'   => 'my_options',
        'icon_url'      => 'dashicons-images-alt',
        'submenu_title' => 'Options', // Note this
    );
    return $settings_pages;
}

add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'             => 'general',
        'title'          => 'General',
        'settings_pages' => 'my-options',
        'fields'         => array(
            array(
                'name' => 'Site Main Action',
                'type' => 'heading',
            ),
						array(
                'name' => 'Title',
                'id'   => 'cta_title',
                'type' => 'textarea',
            ),
						array(
                'name' => 'Content',
                'id'   => 'cta_content',
                'type' => 'textarea',
            ),
						array(
							'name' => 'Button Text',
							'id'   => 'cta_text',
							'type' => 'text',
            ),
						array(
                'name' => 'Button link',
                'id'   => 'cta_link',
                'type' => 'url',
            ),
						array(
							'name' => esc_html__( 'Background Image', 'metabox-online-generator' ),
							'id' => 'cta_bg',
							'type' => 'image_advanced',
							'max_file_uploads' => 1,
						),
					
            
            array(
                'name' => 'Contact Details',
                'type' => 'heading',
            ),
            array(
                'name' => 'Contact Email',
                'id'   => 'email',
                'type' => 'text',
            ),
            array(
                'name' => 'Contact phone Number',
                'id'   => 'phone',
                'type' => 'text',
            ),
            array(
                'name' => 'Address',
                'id'   => 'address',
                'type' => 'textarea',
            ),
						array(
                'name' => 'Map link',
                'id'   => 'map_link',
                'type' => 'url',
            ),
            
            
            array(
                'name' => 'Social Media',
                'type' => 'heading',
            ),
            array(
                'name' => 'Facebook',
                'id'   => 'site_facebook',
                'type' => 'url',
            ),
            array(
                'name' => 'Twitter',
                'id'   => 'site_twitter',
                'type' => 'url',
            ),
            array(
                'name' => 'Instagram',
                'id'   => 'site_instagram',
                'type' => 'url',
            ),
        ),
    );
    return $meta_boxes;
}


