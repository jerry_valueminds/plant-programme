<?php
/*
*=================================================================================================================================
*=================================================================================================================================
*
* RELATIONSHIP SET-UP ( MATCHMAKER ;) )
*
*=================================================================================================================================
*=================================================================================================================================
*/

/*
*
* Intervention to FAQ Post Types
*
*/
add_action( 'mb_relationships_init', function() {
	MB_Relationships_API::register( array(
		'id'   => 'intervention_to_faq',

		'from' => array(
			'object_type' => 'post',
			'post_type'   => 'intervention',
			'meta_box'    => array(
					'title'         => 'FAQs',
					'context'       => 'normal',
			),
		),

		'to' => array(
			'object_type' => 'post',
			'post_type'   => 'faq',
			'meta_box'    => array(
					'title'         => 'Intervention(s) this FAQ belongs to',
					'context'       => 'normal',
					'empty_message' => 'No Interventions created',
			),
		),
	) );
} );

/*
*
* Intervention to Update Post Types
*
*/
add_action( 'mb_relationships_init', function() {
	MB_Relationships_API::register( array(
		'id'   => 'intervention_to_update',

		'from' => array(
			'object_type' => 'post',
			'post_type'   => 'intervention',
			'meta_box'    => array(
					'title'         => 'Updates',
					'context'       => 'normal',
			),
		),

		'to' => array(
			'object_type' => 'post',
			'post_type'   => 'update',
			'meta_box'    => array(
					'title'         => 'Intervention(s) this Update belongs to',
					'context'       => 'normal',
					'empty_message' => 'No Interventions created',
			),
		),
	) );
} );

/*
*
* Intervention to Cohort Post Types
*
*/
add_action( 'mb_relationships_init', function() {
	MB_Relationships_API::register( array(
		'id'   => 'intervention_to_cohort',

		'from' => array(
			'object_type' => 'post',
			'post_type'   => 'intervention',
			'meta_box'    => array(
					'title'         => 'Cohorts',
					'context'       => 'normal',
			),
		),

		'to' => array(
			'object_type' => 'post',
			'post_type'   => 'cohort',
			'meta_box'    => array(
					'title'         => 'Intervention(s) this Cohort belongs to',
					'context'       => 'normal',
					'empty_message' => 'No Interventions created',
			),
		),
	) );
} );
