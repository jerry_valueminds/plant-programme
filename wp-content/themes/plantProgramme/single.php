    <?php get_header() ?>

    <div class="modal fade" id="subscribe">
            <div class="modal-dialog">
                <div class="modal-content">
                    <header class="modal-header">
                        <h4 class="modal-title">Subscribe to Newsletter</h4>
                        <div class="dialogDismiss">
                            <button type="button" class="close_dialog" data-dismiss="modal"></button>
                        </div>
                    </header>
                    <div class="modal-body">
                        <div class="content-panel">
                            <section class="formPane ">
                                <form action="">
                                    <div class="form-group">
                                        <label class="control-label">Your Name</label>
                                        <input type="text" class="form-control" placeholder="Your full name">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Subscibe as a...</label>
                                        <select class="form-control">
                                            <option>Parent</option>
                                            <option>Teacher</option>
                                            <option>School Administrator</option>
                                            <option>Educator</option>
                                            <option>Others</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email Address</label>
                                        <input type="email" class="form-control" placeholder="Enter your email address">
                                    </div>
                                    <div class="form_cta text-right">
                                        <button type="submit" class="btn btn-primary">
                                            Subscribe
                                        </button>
                                    </div>
                                </form>
                            </section>
                            <!-- Hide the form pane and show this after the user submit the form -->
                            <section class="responsePane hidden">
                                <div class="content_wrapper text-center">
                                    <div class="defImg">
                                        <img src="./img/icons/task_done.svg" class="avi_xl" alt="">
                                    </div>
                                    <h3 class="title title_sm margin_bottom_md font-bold">Subscribed</h3>
                                    <p class="font-md">You have been added to our maiing list. Now you can start getting perioding updates of our activities in your inbox.</p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <?php while ( have_posts() ) : the_post(); ?>
    <?php
        $author_id = get_post_field( 'post_author', get_the_ID() );
        $term_list = wp_get_post_terms( $post->ID, 'category', array( 'fields' => 'slugs' ) );
        
    ?>
    <section class="blog_masthead bg__black color__inverse_light">
        <div class="container blogLayoutWrapper">
            <div class="section_header">
                <div class="coloredBar bg__secondary"></div>
                <h4 class="title font-bold caps_upper">The Gradely Blog</h4>
            </div>
            <div class="post_header">
                <div class="postTag">
                    <p class="color__grey_light"><a href="#" class="color__secondary"><?php echo get_the_author(); ?></a> on <time><?php echo get_the_date(); ?></time></p>
                </div>
                <h2 class="title title_xl font-bold margin_bottom_md color__white">
                    <?php the_title() ?>
                </h2>
            </div>
        </div>
    </section>
    <section class="postContent">
        <?php if(has_post_thumbnail()){ ?>
        <div class="featuredImage">
            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); // Featured Image ?>
            <div class="container content_narrow">
                
                <img src="<?php echo $featured_img_url; ?>" alt="Featured Image">
            </div>
        </div>
        <?php } ?>
        <div class="section_block postContentArea">
            <div class="container blogLayoutWrapper">
                <article class="postArticle margin_bottom_lg">
                    <?php echo the_content(); ?>
                </article>
                <div class="postShare postInfoWidget">
                    <h4 class="meta_title caps_upper font-bold">Share Post:</h4>
                    <div class="social_links">
                        <?php echo do_shortcode('[Sassy_Social_Share]') ?>
                        <!--<a href="#" title="Share on Facebook">
                            <svg xmlns="http://www.w3.org/2000/svg" class="cust_icon " viewBox="0 0 64 64">
                                <path d="M45.1,31.8l-8.4,0.1L37,62.6l-12.7,0.1L24,32l-6,0.1l-0.1-10.8l6-0.1l-0.1-7c-0.1-5,2.2-12.8,12.7-13l9.4-0.1L46,11.7l-6.8,0.1c-1.1,0-2.7,0.6-2.7,3l0.1,6.4l9.5-0.1L45.1,31.8z M45.1,31.8" />
                            </svg>
                        </a>
                        <a href="#" title="Share on Twitter">
                            <svg xmlns="http://www.w3.org/2000/svg" class="cust_icon " viewBox="0 0 64 64">
                                <path d="M64,12.2c-2.4,1-4.9,1.8-7.5,2.1c2.7-1.6,4.8-4.2,5.8-7.3c-2.5,1.5-5.3,2.6-8.3,3.2C51.5,7.6,48.1,6,44.3,6c-7.3,0-13.1,5.9-13.1,13.1c0,1,0.1,2,0.3,3C20.6,21.6,10.9,16.3,4.5,8.4c-1.1,1.9-1.8,4.2-1.8,6.6c0,4.6,2.3,8.6,5.8,10.9c-2.2-0.1-4.2-0.7-5.9-1.6c0,0.1,0,0.1,0,0.2c0,6.4,4.5,11.7,10.5,12.9c-1.1,0.3-2.3,0.5-3.5,0.5c-0.8,0-1.7-0.1-2.5-0.2c1.7,5.2,6.5,9,12.3,9.1C15,50.2,9.3,52.3,3.1,52.3c-1.1,0-2.1-0.1-3.1-0.2C5.8,55.8,12.7,58,20.1,58c24.1,0,37.4-20,37.4-37.4c0-0.6,0-1.1,0-1.7C60,17.1,62.2,14.8,64,12.2L64,12.2z M64,12.2" />
                            </svg>
                        </a>
                        <a href="#" title="Share on linkedIn">
                            <svg class="cust_icon " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 58.47 55.88">
                                <path d="M58.47,34.26V55.88H45.94V35.7c0-5.07-1.81-8.52-6.35-8.52a6.86,6.86,0,0,0-6.43,4.58,8.58,8.58,0,0,0-.42,3.06V55.88H20.21s.17-34.16,0-37.7H32.74v5.34l-.08.12h.08v-.12A12.45,12.45,0,0,1,44,17.29C52.29,17.29,58.47,22.68,58.47,34.26ZM7.09,0C2.81,0,0,2.81,0,6.51S2.72,13,6.93,13H7c4.37,0,7.09-2.9,7.09-6.52S11.38,0,7.09,0ZM.74,55.88H13.28V18.17H.74Z"/>
                            </svg>
                        </a>
                        <a href="mailto:" title="Share via email">
                            <svg class="cust_icon " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 18.28">
                                <path d="M21.89,0H2.11A2.11,2.11,0,0,0,0,2.11V16.17a2.11,2.11,0,0,0,2.11,2.11H21.89A2.11,2.11,0,0,0,24,16.17V2.11A2.11,2.11,0,0,0,21.89,0ZM21.6,1.41,12,11,2.41,1.41ZM1.41,15.88V2.39L8.18,9.11Zm1,1L9.18,10.1l2.37,2.35a.7.7,0,0,0,1,0l2.32-2.32,6.74,6.74Zm20.19-1L15.85,9.14,22.59,2.4Z"/>
                            </svg>
                        </a>
                        <a href="" title="Share link">
                            <svg class="cust_icon " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M22.15,10.87l-4.5,4.5a6.3,6.3,0,0,1-10-1.28L9.75,12a1.5,1.5,0,0,1,.34-.23,3.13,3.13,0,0,0,.79,1.36,3.19,3.19,0,0,0,4.5,0l4.5-4.5a3.18,3.18,0,0,0-4.5-4.5l-1.6,1.6a8,8,0,0,0-4.07-.44l3.41-3.41a6.37,6.37,0,0,1,9,9Zm-11.92,7.4L8.62,19.88a3.18,3.18,0,0,1-4.5-4.5l4.5-4.51a3.19,3.19,0,0,1,4.5,0,3.16,3.16,0,0,1,.79,1.34,1.52,1.52,0,0,0,.34-.23l2.09-2.09a6.26,6.26,0,0,0-1-1.28,6.37,6.37,0,0,0-9,0h0L1.87,13.12a6.37,6.37,0,1,0,9,9l3.41-3.41A8,8,0,0,1,10.23,18.27Z"/>
                            </svg>
                        </a>-->
                    </div>
                </div>
                <div class="postAuthor postInfoWidget">
                    <span class="font-default d-block font-bold caps_upper color__black margin_bottom_md">Written By</span>
                    <div class="media_item">
                        <div class="media_figure">
                            <div class="user_info_avi avi_xl">
                                <?php echo get_avatar( get_the_author_email(), '60' ); ?>
                                <div class="default_avi bg__secondary">BO</div>
                            </div>
                        </div>
                        <div class="media_info font-md">
                            <h4 class="title title_sm font-bold color__primary margin_bottom_sm"><?php echo get_the_author(); ?></h4>
                            <p><?php the_author_meta( 'description', $author_id ); ?></p>
                        </div>
                    </div>
                </div>

                <a href="#subscribe" data-toggle="modal" class="postCTA margin_top_md">
                    <p>Get our latest articles in your inbox. <span class="color__secondary font-regular">Sign up for email alerts.</span></p>
                </a>
                <div><?php comments_template() ?></div>
            </div>
        </div>
    </section>
    <?php endwhile; // end of the loop. ?>
    
    <main class="mainContentArea bg__off_white padding-top-none">
        <?php 
            $tax_array = array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $term_list,
                ),
            );
        
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => '3',
                'paged' => 1,
                'tax_query' => $tax_array
            );
            $similar_posts_query = new WP_Query($args);  
        ?>
        <?php if( $similar_posts_query->found_posts > 1 ){ ?>
        <section class="section_block bg__off_white">
            <div class="container">
                <header class="section_header">
                    <h2 class="title title_lg font-bold">Similar Posts</h2>
                </header>
                <div class="postList row">
                <?php 
                    while ($similar_posts_query->have_posts()) : $similar_posts_query->the_post(); 
                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); // Featured Image
                ?>
                    <article class="postThumb has_image col-sm-6 col-md-4">
                        <div class="postThumbWrapper">
                            <div class="postThumbImage">
                                <a href="<?php the_permalink() ?>" class="postImageWrapper">
                                    <img src="<?php echo $featured_img_url; ?>" class="bannerImage" alt="">
                                </a>
                            </div>
                            <div class="postThumbDetails">
                                <div class="listMetaInfo postTag">
                                    <span class="caps_upper color__secondary">
                                        <?php 
                                            $term_list = wp_get_post_terms( $post->ID, 'category', array( 'fields' => 'names' ) );

                                            if( $term_list ){
                                                $counter = 1;
                                                
                                                foreach ( $term_list as $term_item ){
                                                    if( $counter < 2 )
                                                        echo $term_item;
                                                    
                                                    $counter++;
                                                } 
                                            
                                            } 
                                        ?>
                                    </span>
                                    <span class="inline_seperator">•</span>
                                    <span class="">
                                        <time><?php echo get_the_date(); ?></time>
                                    </span>
                                </div>
                                <h2 class="title title_sm font-bold color__primary"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                            </div>
                        </div>
                    </article>
                <?php endwhile; ?>
                
                </div>
            </div>
        </section>
        <?php } ?>

        <?php get_template_part( 'template_parts/footer' ); /* Get Footer */?>

    </main>
    
	<?php get_footer() ?>