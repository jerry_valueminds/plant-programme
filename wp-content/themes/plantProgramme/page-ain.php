<?php /*Template Name: AIN*/ ?>
<?php get_header() ?>
<?php 
	while ( have_posts() ) : the_post();
		$section_one_data = rwmb_meta('ain_page_section_one');
		$section_two_data = rwmb_meta('ain_page_section_two');
		$section_three_data = rwmb_meta('ain_page_section_three');
	endwhile; 
?>

<main class="mainContentArea bg__off_white">
	<section class="section_block bg__inverse padding_bottom_none">
		<div class="container content_narrow">
			<div class="row">
				<div class="offset-md-2 col-md-8 text-center color__white">
					<div class="margin_bottom_sm">
						<img src="<?php echo get_metabox_group_image_url( $section_one_data, 'page_icon' ); ?>" alt="<?php echo get_metabox_group_image_alt( $section_one_data, 'page_icon' ); ?>" class="featureIcon">
					</div>
					<h4 class="title title_md primary font-bold margin_bottom_md"><?php echo $section_one_data['page_title']; ?></h4>
					<p class="font-bold font-md"><?php echo $section_one_data['page_summary']; ?></p>
					<div class="section_cto">
						<a href="<?php echo $section_one_data['sign_up_url']; ?>" class="btn btn-primary" target="_blank">Sign Up Now</a>
					</div>
				</div>
			</div>
			<figure class="floatingBanner">
				<img src="<?php echo get_metabox_group_image_url( $section_one_data, 'page_picture' ); ?>" alt="<?php echo get_metabox_group_image_alt( $section_one_data, 'page_picture' ); ?>">
			</figure>
		</div>
	</section>
	<section class="section_block bg__white">
		<div class="bannerBlobItem homeBlobTopLeft"></div>
		<div class="bannerBlobItem ainBlobBottomRight"></div>
		<div class="container ">
			<div class="row text-center color_text_alt">
				<div class="offset-md-3 col-md-6">
					<header class="margin_bottom_xl margin_top_xl font-md">
						<p><?php echo $section_two_data['summary']; ?></p>
					</header>
				</div>
			</div>
			<?php if( count( $section_two_data['list'] ) ){ ?>
				<div class="row">
				<?php foreach( $section_two_data['list'] as $list_item ){ ?>
					<article class="col-md-4 featureListItem color_text_alt">
						<header class="featHeader featIconHr color__grey_dark">
							<?php if( get_metabox_group_image_url( $list_item, 'list_item_picture' ) ){ ?>
								<div class="feat_Icon">
									<img src="<?php echo get_metabox_group_image_url( $list_item, 'list_item_picture' ); ?>" alt="<?php echo get_metabox_group_image_alt( $list_item, 'list_item_picture' ); ?>" >
								</div>
							<?php } ?>
							<h4 class="featTitle color__black"><?php echo $list_item['list_item_title']; ?></h4>
						</header>
						<p><?php echo $list_item['list_item_content']; ?></p>
					</article>
				<?php } ?>
				</div>
			<?php } ?>

			<div class="section_cto text-center">
				<h4 class="title title_sm font-bold margin_bottom_sm">Join the Agtech Interface Network </h4>
				<a href="<?php echo $section_one_data['sign_up_url']; ?>" target="_blank" class="btn btn-primary">
					Sign Up Now
				</a>
			</div>
		</div>
	</section>
	<section class="tile_block rows no-gutters">
		<div class="tile_item tile_left col-md-7">
			<div class="tile_content_wrapper bg-off-white">
				<div class="content_inner">
					<header class="section_header">
						<h2 class="title title_sm font-bold color__inverse"><?php echo $section_three_data['section_3_title']; ?></h2>
					</header>
					<?php echo $section_three_data['section_3_content']; ?>
					<div class="section_cto">
						<a href="<?php echo $section_three_data['sign_up_url']; ?>" class="btn btn-primary">Sign Up Now</a>
					</div>
				</div>
			</div>
		</div>
		<div class="tile_item tile_right col-md-5">
			<div class="tile_image_wrapper">
				<img src="<?php echo get_metabox_group_image_url( $section_three_data, 'section_3_picture' ); ?>" alt="<?php echo get_metabox_group_image_alt( $section_three_data, 'section_3_picture' ); ?>">
			</div>
		</div>
	</section>
	
	<?php include( locate_template( 'template_parts/upcoming_events.php', false, false ) ); ?>
	
	<section class="section_block bg__white">
		<div class="container content_narrow">
			<div class="row text-center">
				<div class="offset-md-3 col-md-6">
					<header class="margin_bottom_xl">
						<span class="headerSupport caps_upper">FAQ</span>
						<h3 class="title title_md font-bold color__inverse">Have a Question about the AgTech Interface Network?</h3>
					</header>
				</div>
			</div>
			<div class="row">
			<?php 
				$args = array(
					'post_type' => 'faq',
					'post_status' => 'publish',
					'posts_per_page' => 6,
					'meta_query' => array(
						array(
							'key'     => 'topics',
							'value'   => 'ain',
							'compare' => '==',
						),
						array(
							'key'     => 'featured',
							'value'   => '1',
							'compare' => '==',
						),
					)
				);

				$custom_posts = new WP_Query( $args );
			?>
			<?php if ( $custom_posts->have_posts() ) : ?> 
				<?php while ( $custom_posts->have_posts() ) : $custom_posts->the_post(); ?>
				<article class="col-md-6 featureListItem color__grey_dark">
					<h4 class="title title_sm font-bold margin_bottom_sm color__black"><?php echo rwmb_meta( 'question' ) ?></h4>
					<p><?php echo rwmb_meta( 'answer' ) ?></p>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<div class="section_cto text-center">
				<a href="<?php echo get_site_url(); ?>/faqs" class="btn btn-primary">
					See More
				</a>
			</div>
		</div>
	</section>
	
	<?php 
		$feat_updates_params = array( 
			'parent' 						=> 0, 
			'relationship'			=> '', 
			'title'							=> 'AIN Updates', 
			'button_alignment'	=> 'center' 
		); 
	?>
	<?php include( locate_template( 'template_parts/updates.php', false, false ) ); ?>
	<?php include( locate_template( 'template_parts/aim_cta.php', false, false ) ); ?>
</main>

<?php get_footer() ?>
